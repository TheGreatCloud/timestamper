#ifndef FILEMODEL_H
#define FILEMODEL_H

#include <QVector>
#include <QtSql>
#include <memory>
#include "document.h"

/// Represents a folder in the filesystem, provides a wrapper around file operations with a state machine-like behaviour
class DocumentManager
{
public:
    using KeyType = Document::KeyType;

private:
    // Fields
    QSqlDatabase db;
    QString filepath_;
    KeyType selectedDocument;
    bool isDocumentSelected;
	QVector<Document> documents;

public:
    /// Constructs a new wrapper around the given folder
    DocumentManager(QString filepath, QString databaseName = "TimeStamperDocuments.db");
    /// Safely closes all connections
    ~DocumentManager();

    // Accessors
    /// @returns A subset of all stored documents where the filters apply, ordered by the param
    /// @param filter A document is returned only if this string is a substring of at least one of its fields. If empty, matches everything
    /// @param searchFlags which fields to look at and ignore
    /// @param orderBy A list of fields to sort the documents by
	QVector<Document> getDocuments(const QString& filter = "", int searchFlags = 0, const QStringList& orderBy = {}) const;
    /// @returns The document identified by the given key, or nullptr if none exist
    const Document* getDocument(KeyType key) const;
    /// @returns Whether a document identified by the given key exist
    bool documentWithKeyExists(KeyType filename);
    /// @returns The document that has previously been flagged as open, or nullptr if setNoOpenDocument was issued last
    const Document* getOpenDocument() const;
    /// @returns The root directory of the database (exclusing the database name)
    const QString& getPath() const {return filepath_;}

    // Input
    /// Stores the document in memory and the database
    /// @returns Whether storing the document was successful
    /// @param doc The document to store
    /// @param setAsOpen Whether to also flag this document as the currently opened document
    bool storeDocument(const Document& doc, bool setAsOpen = true);
    /// Sets the given document as the currently open document
    void setOpenDocument(KeyType key);
    /// Sets the state that no existing document is being edited
    void setNoOpenDocument();
    /// Removes a document from memory and the database identified by the given key
    /// @note No-op if no document exists with the given key
    void deleteDocument(KeyType key);
    /// Deletes every document stored in the database.
    /// @returns Whether the SQL operations were successful
    bool deleteAllDocuments();
    /// Copies the working database file to the specified location
    /// @returns Whether the copy was successfully created
    bool createBackup(const QString& path = "./TimeStamperDocuments_Backup.db");

    // Extras
    /// Writes the received text into the specified file.
    /// @returns whether the write was successful.
    bool writeToFile(const QString& text, const QString& filename) const;
};

#endif // FILEMODEL_H
