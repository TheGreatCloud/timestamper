#include "documentmanager.h"
#include <QDir>
#include <QDataStream>
#include <QFile>
#include <QTextStream>

#include <QDebug>

#include <stdexcept>

DocumentManager::DocumentManager(QString filepath, QString databaseName) :
    db(QSqlDatabase::addDatabase("QSQLITE")),
    filepath_(filepath),
    isDocumentSelected(false)
{
    QString databasePath = filepath_ + "/" + databaseName;

    // create path if it doesn't exist
    QDir dir(filepath_);
    if (!dir.exists())
        if(!dir.mkpath("."))
            throw std::runtime_error("Could not create nonexistant folder " + filepath_.toStdString());

    // open database
    db.setDatabaseName(databasePath);
    db.open();
    if(!db.isOpen())
        throw std::runtime_error("Could not open database at " + databasePath.toStdString());

    // create tables if they are missing
    if (!db.tables().contains( QLatin1String("document_headers")))
    {
        qDebug() << "Table document_headers does not exist! Creating...";
		QSqlQuery query;
        if(!query.exec("CREATE TABLE document_headers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, epoch TEXT NOT NULL, title TEXT NOT NULL, subtitle TEXT NOT NULL, description TEXT NOT NULL, numbering TEXT NOT NULL, numbering_start INTEGER NOT NULL)"))
            qDebug() << "Problem occurred setting up database table document_headers: " << query.lastError().text();
    }
    if(!db.tables().contains(QLatin1String("document_entries")))
    {
        qDebug() << "Table document_entries does not exist! Creating...";
        QSqlQuery query;
        if(!query.exec("CREATE TABLE document_entries (document_id INTEGER NOT NULL, entry_id INTEGER NOT NULL, time TEXT NOT NULL, title TEXT NOT NULL, body TEXT NOT NULL, PRIMARY KEY(document_id, entry_id))"))
            qDebug() << "Problem occurred setting up database table document_entries: " << query.lastError().text();
    }
    db.commit();

	// read information into memory
	QSqlQuery query(db);
	query.exec("SELECT * FROM document_headers");
	while(query.next())
	{
		Document doc{query.value("title").toString(),
					query.value("subtitle").toString(),
					query.value("description").toString(),
					query.value("epoch").toDateTime()
					};
		doc.key = query.value("id").toString();
        doc.numbering = query.value("numbering").toString();
        doc.numberingStart = query.value("numbering_start").toInt();
		documents.push_back(doc);
	}
	query.exec("SELECT * FROM document_entries ORDER BY entry_id");
	while(query.next())
	{
		Document::Entry e;
		e.time = query.value("time").toTime();
		e.title = query.value("title").toString();
		e.body = query.value("body").toString();
		Document* doc = const_cast<Document*>(getDocument(query.value("document_id").toString()));
		if(doc)
		{
			// SQL query is ordered by entry ID, so this is fine
			doc->entries.push_back(e);
		}
	}
	// just to be safe
	setNoOpenDocument();
}

DocumentManager::~DocumentManager()
{
    db.commit();
    db.close();
}

void tryInsert(QVector<Document>& docs, const Document& doc, const QStringList& orderBy)
{
    for(int i = 0; i < docs.size(); ++i)
        if(doc.isLessThan(docs.at(i), orderBy))
        {
            docs.insert(i, doc);
            return;
        }
    docs.push_back(doc);
}

QVector<Document> DocumentManager::getDocuments(const QString &filter, int searchFlags, const QStringList &orderBy) const
{
	QVector<Document> docs;
	for(const Document& doc : documents)
		if(doc.containsText(filter, searchFlags))
            tryInsert(docs, doc, orderBy);
    return docs;
}

const Document* DocumentManager::getDocument(DocumentManager::KeyType key) const
{
	for(const Document& doc : documents)
		if(doc.key == key)
			return &doc;
	return nullptr;
}

bool DocumentManager::documentWithKeyExists(DocumentManager::KeyType filename)
{
	return getDocument(filename);
}

const Document* DocumentManager::getOpenDocument() const
{
    if(isDocumentSelected)
        return getDocument(selectedDocument);
    else
		return nullptr;
}

bool DocumentManager::storeDocument(const Document &doc, bool setAsOpen)
{
    // insert header into database
    QSqlQuery queryHeader(db);
	if(isDocumentSelected)
	{
        queryHeader.prepare("UPDATE document_headers SET epoch = :epoch, title = :title, subtitle = :subtitle, description = :description, numbering = :numbering, numbering_start = :numstart WHERE id = :key");
        queryHeader.bindValue(":key", selectedDocument);

        // delete existing entries
        QSqlQuery deleteEntries(db);
        deleteEntries.prepare("DELETE FROM document_entries WHERE document_id = :key");
        deleteEntries.bindValue(":key", selectedDocument);
        if(!deleteEntries.exec())
            qDebug() << deleteEntries.lastError().text();
	}
	else
    {
        queryHeader.prepare("INSERT INTO document_headers (epoch, title, subtitle, description, numbering, numbering_start) VALUES(:epoch, :title, :subtitle, :description, :numbering, :numstart)");
	}
    queryHeader.bindValue(":epoch", doc.epoch);
    queryHeader.bindValue(":title", doc.title);
    queryHeader.bindValue(":subtitle", doc.subtitle);
    queryHeader.bindValue(":description", doc.text);
    queryHeader.bindValue(":numbering", doc.numbering);
    queryHeader.bindValue(":numstart", doc.numberingStart);
    bool result = queryHeader.exec();
    if(!result)
        qDebug() << "Error while inserting header:\n" << doc.epoch << doc.title << doc.subtitle << doc.text << "\n" << queryHeader.lastError().text();

    // get key for stored document
    KeyType key = selectedDocument;
    if(!this->isDocumentSelected)
    {
        QSqlQuery getId(db);
        result &= getId.exec("SELECT MAX(Id) FROM document_headers");
        getId.next();
        key = getId.value(0).toString();
    }

    // write entries for stored document into database
    for(int i = 0; i < doc.entries.size(); ++i)
    {
        const Document::Entry& entry = doc.entries.at(i);
        QSqlQuery query(db);
        query.prepare("INSERT INTO document_entries VALUES(:docid, :entryid, :time, :title, :text)");
        query.bindValue(":docid", key.toInt());
        query.bindValue(":entryid", i);
        query.bindValue(":time", entry.time);
        query.bindValue(":title", entry.title);
        query.bindValue(":text", entry.body);
		bool b = query.exec();
		result &= b;
		if(!b)
            qDebug() << query.lastError().text();
    }
    db.commit();

    // inserts document into model stored in memory
    if(isDocumentSelected)
    {
        for(Document& docum : documents)
            if(docum.key == key)
                docum.makeEqualTo(doc);
    }
    else
    {
        documents.push_back(doc);
        documents.last().key = key;
    }

	// set flags and status
    if(setAsOpen)
		setOpenDocument(key);

    return result;
}

void DocumentManager::setOpenDocument(DocumentManager::KeyType key)
{
	selectedDocument = key;
	isDocumentSelected = true;
    qDebug() << "selecting document" << key;
}

void DocumentManager::setNoOpenDocument()
{
	isDocumentSelected = false;
    qDebug() << "deselecting document";
}

void DocumentManager::deleteDocument(DocumentManager::KeyType key)
{
	// remove from database
	QSqlQuery query(db);
	query.prepare("DELETE FROM document_headers WHERE id = :key");
	query.bindValue(":key", key);
	query.exec();
	db.commit();

	// remove from memory
	for(int i = 0; i < documents.size(); ++i)
		if(documents.at(i).key == key)
            documents.removeAt(i);

    // Set no open document if necessary
    if(isDocumentSelected && selectedDocument == key)
        setNoOpenDocument();
}

bool DocumentManager::deleteAllDocuments()
{
    QSqlQuery query(db);
    bool result = true;
    if(!query.exec("DELETE FROM document_headers"))
    {
        result = false;
        qDebug() << query.lastError();
    }
    if(!query.exec("DELETE FROM document_entries"))
    {
        result = false;
        qDebug() << query.lastError();
    }
    documents.clear();
    setNoOpenDocument();
    return result;
}

bool DocumentManager::createBackup(const QString &path)
{
    if(QFileInfo(path) == QFileInfo(db.databaseName()))
        return false;
    QFile database(db.databaseName());
	if(QFile(path).exists())
		QFile(path).remove();
    return database.copy(path);
}

bool DocumentManager::writeToFile(const QString &text, const QString &filename) const
{
	QFile f(filename);
	if(!f.open(QIODevice::WriteOnly | QIODevice::Text))
		return false;
	QTextStream out(&f);
	out << text;
	f.close();
	return true;
}
