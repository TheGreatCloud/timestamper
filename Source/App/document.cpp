#include "document.h"

#include <QTextStream>
#include <QDebug>

QString Document::toMarkdown(QString dateStyle) const
{
	QString str;
	QTextStream out(&str);
	out << "# " << title << '\n';
	if(subtitle.size() > 0)
		out << subtitle << '\n';
    out << '\n' << epoch.toString(dateStyle) << "\n\n";
    if(text.length() > 0)
        out << "# Description\n\n" << text;
    if(entries.length() > 0)
        out << "\n\n# Contents";
	for(Document::Entry e : entries)
    {
        out << "\n\n## " << e.title << "\n" << e.time.toString("hh:mm:ss");
        if(e.body.length() > 0)
            for(QStringRef str : e.body.splitRef("\n"))
                out << "\n\n" << str;
    }
    out << "\n\nSaved at " << QDateTime::currentDateTime().toString(dateStyle);
	return str;
}

QString Document::toPlainText(QString dateStyle) const
{
	QString str;
	QTextStream out(&str);
	out << title << '\n';
	if(subtitle.size() > 0)
		out << subtitle << '\n';
    out << '\n' << epoch.toString(dateStyle) << "\n\n";
	out << text << "\n\n";
	for(Document::Entry e : entries)
	{
		if(e.title.length() > 0)
			out << e.title << "\n";
		else
			out << "Untitled section\n";
		out << e.time.toString("hh:mm:ss") << "\n";
		if(e.body.length() > 0)
			out << e.body << "\n";
		out << "\n";
	}
	out << "Saved at " << QDateTime::currentDateTime().toString(dateStyle);
	return str;
}

/// Helper function for searchDocumentText
inline void search(const QString& in, const QString& what, int rowid, Document::SearchResult::Type type, QVector<Document::SearchResult>& storeIn)
{
    for(auto found = in.toLower().indexOf(what.toLower());
        found >= 0;
        found = in.toLower().indexOf(what.toLower(), found + 1))
		storeIn.push_back({rowid, type, static_cast<int>(found)});
}

QVector<Document::SearchResult> Document::searchDocumentText(QString str, int searchFlags) const
{
    QVector<SearchResult> results;
	if(searchFlags == 0)
		searchFlags = 0b1111111111111111111111111111111;
    if(searchFlags & static_cast<int>(Document::searchResultType::epoch))
    {
        // only store one result for time
        if(epoch.toString().toLower().indexOf(str.toLower()) >= 0)
            results.push_back({-1, SearchResult::Type::time, 0});
    }
    if(searchFlags & static_cast<int>(Document::searchResultType::title))
        search(title, str, -1, SearchResult::Type::title, results);
    if(searchFlags & static_cast<int>(Document::searchResultType::subtitle))
        search(subtitle, str, -1, SearchResult::Type::subtitle, results);
    if(searchFlags & static_cast<int>(Document::searchResultType::text))
        search(text, str, -1, SearchResult::Type::body, results);
	if(searchFlags & static_cast<int>(Document::searchResultType::entries))
    {
        for(int i = 0; i < entries.size(); ++i)
        {
            const Entry& e = entries.at(i);
            // only store one result for time
            if(e.time.toString().indexOf(str) >= 0)
                results.push_back({i, Document::SearchResult::Type::time, 0});
            search(e.title, str, i, Document::SearchResult::Type::title, results);
            search(e.body, str, i, Document::SearchResult::Type::body, results);
        }
    }
	return results;
}

/// Helper function for containsText
inline bool searchString(const QString& where, const QString& what)
{
    return where.toLower().indexOf(what.toLower()) >= 0;
}

bool Document::containsText(QString str, int searchFlags) const
{
	if(str.length() < 1)
		return true;
	if(searchFlags == 0)
		searchFlags = 0b1111111111111111111111111111111;
	if(searchFlags & static_cast<int>(Document::searchResultType::epoch))
    {
        if(searchString(epoch.toString(), str))
			return true;
	}
    if(searchFlags & static_cast<int>(Document::searchResultType::title) && searchString(title, str))
		return true;
    if(searchFlags & static_cast<int>(Document::searchResultType::subtitle) && searchString(subtitle, str))
		return true;
    if(searchFlags & static_cast<int>(Document::searchResultType::text) && searchString(text, str))
		return true;
	if(searchFlags & static_cast<int>(Document::searchResultType::entries))
	{
		for(const Entry& e : entries)
		{
            if(searchString(e.time.toString(), str))
				return true;
            if(searchString(e.title, str))
				return true;
            if(searchString(e.body, str))
				return true;
		}
	}
    return false;
}

bool Document::equals(const Document &other) const
{
    bool b = epoch == other.epoch &&
            title == other.title &&
            subtitle == other.subtitle &&
            text == other.text &&
            entries.length() == other.entries.length();
    for(int i = 0; i < entries.length(); ++i)
        b &= entries.at(i).equals(other.entries.at(i));
    return b;
}

Document& Document::operator=(const Document &other)
{
    key = other.key;
    return makeEqualTo(other);
}

Document &Document::makeEqualTo(const Document &other)
{
    epoch = other.epoch;
    title = other.title;
    subtitle = other.subtitle;
    text = other.text;
    entries = other.entries;
    numbering = other.numbering;
    numberingStart = other.numberingStart;
    return *this;
}

bool Document::isLessThan(const Document &other, const QStringList &orderBy) const
{
    for(int i = 0; i < orderBy.length(); ++i)
    {
        if(orderBy.at(i) == "Title (Ascending)")
        {
            if(other.title > title)
                return true;
            if(other.title < title)
                return false;
        }
        if(orderBy.at(i) == "Title (Descending)")
        {
            if(other.title < title)
                return true;
            if(other.title > title)
                return false;
        }
        if(orderBy.at(i) == "Epoch (Ascending)")
        {
            if(other.epoch > epoch)
                return true;
            if(other.epoch < epoch)
                return false;
        }
        if(orderBy.at(i) == "Epoch (Descending)")
        {
            if(other.epoch < epoch)
                return true;
            if(other.epoch > epoch)
                return false;
        }
    }
    //qDebug() << "Can't determine whether two documents are equal:\n" << toPlainText() << "\n" << other.toPlainText();
    return false;
}
