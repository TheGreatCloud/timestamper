#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
	Q_OBJECT

	// UI elements
	Ui::SettingsDialog *ui;

public:
    /// Switches to the tab with the given title
	void goToPage(QString page);

	explicit SettingsDialog(class MainWindow& parent);
	~SettingsDialog();

private:
    /// Writes the information to QSettings
    void saveSettings();

signals:
	/// Signals to anyone who may want to load the settings
	void settingSaved();
	void closed();

private slots:
    void on_buttonBox_accepted();
    void on_btnApply_clicked();
    void on_btnInnerSaveFileLocation_clicked();
	void on_listWidgetSortBy_itemDoubleClicked(class QListWidgetItem* item);
    void on_spinBoxButtonMaxWidth_valueChanged(int arg1);
    void on_txtNumberingWildcardLineEdit_textEdited(const QString &arg1);
	void on_SettingsDialog_finished(int);
};

#endif // SETTINGSDIALOG_H
