#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTime>

#include "document.h"
#include "documentmanager.h"
#include "widgets.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

	// View
	Ui::MainWindow *ui;
	QVector<DocumentEntry*> timeStampList;
    QVector<QPushButtonFile*> buttons;
	class SettingsDialog* settingsWindow;

	// Model
    std::unique_ptr<DocumentManager> fileSystem;
    std::unique_ptr<QVector<Document::SearchResult>> searchResults;
	QLineEdit* lastSelectedLineEdit;
	QTextEdit* lastSelectedTextEdit;

	enum class textEdits {none, line, text};
	textEdits lastSelectedEdit;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private:
	/// Creates a new document and sets it as working document
    void createNewDocument();
    /// Attempts to save the currently edited document, showing error messages upon failure
	void saveCurrentDocument();
	/// Exports current contents of document to file in markdown format
	/// @param customSavePath File to export to. If empty, asks user with popup
	void exportAsMarkdown(QString customSavePath = "");
	/// Exports current contents of document to file in plain text
	/// @param customSavePath File to export to. If empty, asks user with popup
	void exportAsPlainText(QString customSavePath = "");

    // getters
    /// Constructs a new data structure from the UI fields
    /// @returns a Document object containing all the information of the current working document
    Document makeDocumentFromView() const;
    /// @returns An absolute path for quickly getting a save file path
	QString generateFullPathFromUI(QString extension) const;
    /// @returns Whether discarding the currently working document would result in a loss of data
    bool doUnsavedChangesExist() const;

	// settings
    /// Loads settings that can be changed during the execution of the program
	void loadChangeableSettings();
	/// Sets all settings to a reasonable default, then loads changeable settings
	void resetSettingsToDefault();

    // Modify current document (and UI)
	/// Appends a new entry to the end of the existing entries
    /// @returns The created entry
	DocumentEntry* addNewTimeStamp(Document::Entry e = {QTime(0, 0), "", ""}, bool redrawUI = true);
    /// Appends a new line at the end of the last entry's body
    void insertSmallTimeStamp(QTime t = QTime(0, 0, 0));
    /// Appends a new entry to the end of the existing entries with a special title
    void insertTimeStampImportant();
    /// Inserts a symbol to current cursor
    /// @param str A representation of the symbol
	void insertSymbol(QString str);
    /// Inserts a new entry before the entry with the given index
    /// @returns The created entry
    DocumentEntry* insertTimeStampBefore(int timeStampPosition, bool redrawUI = true);

	// widgets and UI
    /// updates the Document Browser from the data layer
	void updateLoadableDocuments();
    /// Sets the UI labels to current information
    void updateReadOnlyWidgets();
    /// Sets a UI element to display current time
	void insertFellAsleepTimeStamp();
    /// Hides the search panel on the bottom of the window and restores focus to editor
	void hideSearchWidget();
    /// Shows the search panel on the bottom of the window
	/// @param switchFocus whether to also move focus to its lineEdit field
	void showSearchWidget(bool switchFocus = false);
	/// Highlights the specified position within the current working document
	void showSearchResult(int i);
    /// Opens the settings window in a new window
	void openSettings(const QString& page);
    /// Sets whether the 'document settings' are shown or hidden
    void setSettingsVisibility(bool visible);

public slots:
    /// Deletes the given entry from view
    void deleteEntry(DocumentEntry* e);
    /// Reloads settings and stored documents
    void loadSettings();
    /// Closes currently open document and loads specified one
    void loadDocument(DocumentManager::KeyType key);
    /// Deletes the given file from the filesystem, then updates UI accordingly
    void deleteDocument(DocumentManager::KeyType key);
    /// Remembers which text edit widget was last selected
    void saveFocusedWidgets(QWidget* from, QWidget*);
	/// Allows for the settings window to be opened again
	void reEnableSettings();

private slots:
	void on_escapePressed();
    virtual void closeEvent(QCloseEvent*event) override;

	void on_btnSetEpoch_clicked();
	void on_btnTimeStamp_clicked();
    void on_actionExport_to_file_triggered();
	void on_actionExport_to_new_file_triggered();
	void on_actionExit_triggered();
	void on_btnInsertSymbol_clicked();
	void on_btnTimeInsertMode_clicked();
	void on_actionPreferences_triggered();
	void on_actionRestoreDefaultDocument_triggered();
	void on_actionExport_to_Markdown_triggered();
	void on_actionAbout_me_triggered();
	void on_actionDocument_browser_widget_triggered(bool checked);
	void on_actionSearch_current_document_triggered();
	void on_btnSearchClose_clicked();
	void on_txtSearchSimple_returnPressed();
	void on_btnSearchNext_clicked();
	void on_btnSearchPrev_clicked();
    void on_txtSearchSimple_textChanged(const QString &);
    void on_txtDocumentBrowserSearch_textChanged(const QString);
	void on_btnSearchConfig_clicked();
	void on_actionSearch_all_documents_triggered();
	void on_textEdit_textChanged();
    void on_btnTImeStampSmall_clicked();
    void on_actionHow_to_use_triggered();
	void on_actionReset_to_default_settings_triggered();
    void on_actionExport_All_triggered();
    void on_btnEditDocumentFields_clicked();
    void on_btnNewDocument_clicked();
    void on_btnSaveDocument_clicked();
    void on_btnSearch_clicked();
    void on_btnAddTimestampTextEditor_clicked();
    void on_btnSearchThis_clicked();
    void on_actionExport_as_PlainText_triggered();
    void on_spinBoxSearchResult_editingFinished();
    void on_btnOpenDocumentOptions_clicked();
    void on_txtDocumentNumberScheme_textChanged(const QString &arg1);
    void on_spinBoxNumberingStartsFrom_valueChanged(int);
    void on_actionCredits_triggered();
    void on_actionDelete_all_documents_triggered();
    void on_actionCreate_backup_triggered();
    void on_actionRestore_backup_triggered();
    void on_txtDocumentBrowserSearch_returnPressed();
    void on_splitter_splitterMoved(int, int);
    void on_actionInsert_timestamp_before_entry_triggered();
};

#endif // MAINWINDOW_H
