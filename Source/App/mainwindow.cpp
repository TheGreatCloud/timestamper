#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"

#include <QDateTime>
#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QShortcut>
#include <QTime>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QSettings>
#include <QKeyEvent>
#include <QDir>
#include <QDesktopServices>
#include <QLineEdit>
#include <QTimeEdit>

#include <algorithm>

#include "symbols.h"
#include "document.h"
#include "widgets.h"

Q_DECLARE_METATYPE(QList<int>) // to store splitter sizes

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
	ui(new Ui::MainWindow),
	settingsWindow(nullptr)
{
	ui->setupUi(this);

    // load basic settings
	QSettings setting("Cloud", "TimeStamper");
	if(setting.value("TestValue").toString() != "TestValue")
        resetSettingsToDefault();
    if(setting.value("WindowShowMaximized").toBool())
        showMaximized();
    ui->lblRightInfo->setStyleSheet("QLabel { color: gray }");
    ui->lblLeftInfo->setStyleSheet ("QLabel { color: rgb(100, 100, 100) }");
    ui->lblStillGoingInfo->setStyleSheet("QLabel { color: rgb(100, 100, 100) }");
    loadChangeableSettings();
    ui->splitter->setCollapsible(0, false);
    setSettingsVisibility(false);
    ui->spinBoxNumberingStartsFrom->setValue(1);
    ui->splitter->setSizes(setting.value("SplitterPosition").value<QList<int> >());
    hideSearchWidget();

    // open data layer
    try
    {
        fileSystem = std::make_unique<DocumentManager>(setting.value("InnerSaveFileLocation").toString());
    }
    catch(std::exception& e)
    {
        QMessageBox::warning(this, "Error", e.what());
		if(QMessageBox::question(this, "Reset to default settings?", "Do you want to reset the program into its default state? Maybe this will help.") == QMessageBox::Yes)
			resetSettingsToDefault();
    }

    // initialize data layer and self with data
    updateLoadableDocuments();
    createNewDocument();

    // shortcuts
	// set up ctrl+i important stamp
	QShortcut *shortcutImportant = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_I), this);
	QObject::connect(shortcutImportant,    &QShortcut::activated,
                     this,        &MainWindow::insertTimeStampImportant);
	// set up ctrl+l still going
	QShortcut *shortcutStillGoing = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_L), this);
	QObject::connect(shortcutStillGoing,    &QShortcut::activated,
					 this,        &MainWindow::insertFellAsleepTimeStamp);
	// set up escape
	QShortcut *shortcutEscapePressed = new QShortcut(QKeySequence(Qt::Key_Escape), this);
	QObject::connect(shortcutEscapePressed,    &QShortcut::activated,
					 this,        &MainWindow::on_escapePressed);
	// set up focus change catching
	connect(QApplication::instance(), SIGNAL(focusChanged(QWidget*, QWidget*)), this, SLOT(saveFocusedWidgets(QWidget*, QWidget*)));
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::createNewDocument()
{
    QSettings setting("Cloud", "TimeStamper");

    // overwrite current document
    ui->textEdit->setText("");
    ui->lblStillGoingInfo->setText("");
    ui->dateTimeEditEpoch->setDateTime(QDateTime::currentDateTime());
    ui->txtTitle->setText(setting.value("DefaultDocumentTitle").toString());
    ui->txtSubtitle->setText(setting.value("DefaultDocumentSubtitle").toString());
	for(DocumentEntry* p : timeStampList)
		delete p;
    timeStampList.clear();
    updateReadOnlyWidgets();
    hideSearchWidget();
    fileSystem->setNoOpenDocument();
    ui->txtDocumentNumberScheme->setText("");
    ui->spinBoxNumberingStartsFrom->setValue(1);
    ui->lblRightInfo->setText("Document created!");
    // Without this line, a crash can occur because the buttons aren't refreshed yet
    qApp->processEvents();
    for(auto* p : buttons)
    {
        p->setBold(false);
    }
}

void MainWindow::loadDocument(DocumentManager::KeyType key)
{
    static bool inUse = false;
    if(inUse)
        return;
    inUse = true;
    if(doUnsavedChangesExist() && QMessageBox::question(this, "Loading document", "Are you sure you want to discard all unsaved changes?",
                                       QMessageBox::Yes|QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
    {
        inUse = false;
        return;
    }
    if(!fileSystem->documentWithKeyExists(key))
        QMessageBox::information(this, "Error", "Error while reading file " + key);

    // reset view
    for(DocumentEntry* p : timeStampList)
        delete p;
    timeStampList.clear();
    ui->lblRightInfo->setText("");
    hideSearchWidget();

    // query data layer
	const Document* doc = fileSystem->getDocument(key);
    fileSystem->setOpenDocument(key);

    // handle WTF error
    if(!doc)
    {
        qDebug() << "This shouldn't have happened, but the document cannot be found (key:" << key << ")";
        QMessageBox::warning(this, "Error", "Document cannot be loaded, there is no document with key " + key);
    }

    // load actual contents
    ui->dateTimeEditEpoch->setDateTime(doc->epoch);
    ui->txtTitle->setText(doc->title);
    ui->txtSubtitle->setText(doc->subtitle);
    ui->textEdit->setText(doc->text);
    DocumentEntry* previousEntry = nullptr;
    DocumentEntry* ent = nullptr;
    for(const Document::Entry& e : doc->entries)
    {
        previousEntry = ent;
        ent = this->addNewTimeStamp(e, false);
        if(previousEntry)
            ent->chainAfter(*previousEntry);
        else
            ent->chainAfter(ui->textEdit);

    }

    // load Document-specific settings
    ui->txtDocumentNumberScheme->setText(doc->numbering);
    ui->spinBoxNumberingStartsFrom->setValue(doc->numberingStart);

    // Process settings
    // Without processEvents, the display will be skewed because QTextEdit::Document displays wrong size
    qApp->processEvents();
    loadChangeableSettings();
    updateReadOnlyWidgets();
    ui->lblRightInfo->setText("Document loaded!");
    for(auto* p : buttons)
    {
        p->setBold(p->getKey() == doc->key);
    }
    inUse = false;
}

Document MainWindow::makeDocumentFromView() const
{
    Document currentDocument;
    currentDocument.key = fileSystem->getOpenDocument() ? fileSystem->getOpenDocument()->key : "";
    currentDocument.text = ui->textEdit->toPlainText();
    currentDocument.epoch = ui->dateTimeEditEpoch->dateTime();
    currentDocument.title = ui->txtTitle->text();
    currentDocument.subtitle = ui->txtSubtitle->text();
	for(DocumentEntry* e : timeStampList)
		currentDocument.entries.push_back({e->getTime(), e->getTitle(), e->getBody()});
    currentDocument.numbering = ui->txtDocumentNumberScheme->text();
    currentDocument.numberingStart = ui->spinBoxNumberingStartsFrom->value();
    return currentDocument;
}

void MainWindow::saveCurrentDocument()
{
    // Handle clicking way too quickly
    static bool inUse = false;
    if(inUse)
    {
        QMessageBox::information(this, "Warning", "Already saving, please don't do this to me!");
        return;
    }
    inUse = true;

    // Save document
    try
    {
        Document currentDocument = makeDocumentFromView();
        bool success = fileSystem->storeDocument(currentDocument);
        if(success)
        {
            ui->lblRightInfo->setText("Last saved: " + QTime::currentTime().toString());
            updateLoadableDocuments();
        }
        else
            ui->lblRightInfo->setText("Unsuccessful save at: " + QTime::currentTime().toString());
    }
    catch(std::exception& e)
    {
        QMessageBox::warning(this, "Error while saving file", e.what());
    }
    catch(...) {}

    // Re-enable current function
    inUse = false;
}

void MainWindow::deleteDocument(DocumentManager::KeyType key)
{
    if(!fileSystem->documentWithKeyExists(key))
        QMessageBox::information(this, "Deletion unsuccessful", "No document by the name " + key + " was found.");
    else
    {
        if(QMessageBox::question(this, "Delete?", "Are you sure you want to delete this document?") == QMessageBox::No)
            return;
        fileSystem->deleteDocument(key);
        updateLoadableDocuments();
	}
}

void MainWindow::saveFocusedWidgets(QWidget *from, QWidget *)
{
    // Only save QLineEdit and QTestEdit, everything else is ignored
	if(dynamic_cast<QLineEdit*>(from))
	{
		lastSelectedLineEdit = dynamic_cast<QLineEdit*>(from);
		lastSelectedEdit = textEdits::line;
	}
	else if(dynamic_cast<QTextEdit*>(from))
	{
		lastSelectedTextEdit = dynamic_cast<QTextEdit*>(from);
		lastSelectedEdit = textEdits::text;
	}
}

void MainWindow::reEnableSettings()
{
	settingsWindow = nullptr;
	// Re-enable nevertheless of setting, this may prevent future bugs or provide a workaround
	this->setEnabled(true);
}

QString MainWindow::generateFullPathFromUI(QString extension) const
{
	return fileSystem->getPath() + "/" + ui->txtTitle->text() + " " +
            ui->dateTimeEditEpoch->dateTime().toString("yyyy MM dd hh-mm-ss") + "." + extension;
}

bool MainWindow::doUnsavedChangesExist() const
{
	const Document* orig = fileSystem->getOpenDocument();

    // Document is not saved to model
    if(!orig)
    {
        QSettings setting("Cloud", "TimeStamper");
        return ui->textEdit->toPlainText().length() > 0 ||
                timeStampList.length() > 0 ||
                ui->txtTitle->text() != setting.value("DefaultDocumentTitle").toString() ||
                ui->txtSubtitle->text() != setting.value("DefaultDocumentSubtitle").toString();
    }

    // Document is saved to model
    bool b = orig->title != ui->txtTitle->text() ||
            orig->subtitle != ui->txtSubtitle->text() ||
            orig->text != ui->textEdit->toPlainText() ||
            orig->epoch != ui->dateTimeEditEpoch->dateTime() ||
            orig->entries.length() != timeStampList.length();

    if(b)
        return true;
    for(int i = 0; i < timeStampList.length() && i < orig->entries.length(); ++i)
        if(orig->entries.at(i).time != timeStampList.at(i)->getTime() ||
                orig->entries.at(i).title != timeStampList.at(i)->getTitle() ||
                orig->entries.at(i).body != timeStampList.at(i)->getBody())
            return true;
    return false;
}

void MainWindow::exportAsMarkdown(QString customSavePath)
{
	if(customSavePath == "")
        customSavePath = QFileDialog::getSaveFileName(this, "Choose file location",
                                                      generateFullPathFromUI("md"));
    // Handle cancel clicked
    if(customSavePath == "")
		return;

	// a filepath is assumed to exist by this point
    QSettings setting("Cloud", "TimeStamper");
    if(fileSystem->writeToFile(makeDocumentFromView().toMarkdown(setting.value("DefaultDocumentDateStyle").toString()), customSavePath))
		QMessageBox::information(this, "Success!", "Saving successful!");
	else
		QMessageBox::information(this, "Failure!", "Cloud not save file! Is it a valid filename?");
}

void MainWindow::exportAsPlainText(QString customSavePath)
{
	if(customSavePath == "")
		customSavePath = QFileDialog::getSaveFileName(this, "Choose file location",
                                                      generateFullPathFromUI("txt"));
	if(customSavePath == "")
		return;
	// a filepath is assumed to exist by this point
    QSettings setting("Cloud", "TimeStamper");
    if(fileSystem->writeToFile(makeDocumentFromView().toPlainText(setting.value("DefaultDocumentDateStyle").toString()), customSavePath))
		QMessageBox::information(this, "Success!", "Saving successful!");
	else
		QMessageBox::information(this, "Failure!", "Cloud not save file! Is it a valid filename?");
}

void MainWindow::loadChangeableSettings()
{
	// use these to restore conditions later:
	auto cursor = ui->textEdit->textCursor();

    QSettings setting("Cloud", "TimeStamper");

    // set document font
	auto font = setting.value("MainTextFont").value<QFont>();
	font.setPointSize(setting.value("MainTextSize").toInt());
    QFont fontLabel = font;
    fontLabel.setPointSize(font.pointSize() - 2);
    int titleHeight = static_cast<int>(QFontMetrics(font).height()) + 3;
    ui->lblDocumentSettingsDisplay->setFont(fontLabel);
    ui->lblDocumentNumberScheme->setFont(fontLabel);
    ui->txtDocumentNumberScheme->setFont(font);
	ui->textEdit->setFont(font);
    ui->dateTimeEditEpoch->setFont(font);
	ui->dateTimeEditEpoch->setFixedHeight(titleHeight);
    ui->txtTitle->setFont(font);
	ui->txtTitle->setFixedHeight(titleHeight);
    ui->txtSubtitle->setFont(font);
	ui->txtSubtitle->setFixedHeight(titleHeight);
	ui->btnOpenDocumentOptions->setFixedSize(titleHeight, titleHeight);
    ui->lblStartFrom->setFont(fontLabel);
    ui->spinBoxNumberingStartsFrom->setFont(font);
    for(int i = 0; i < timeStampList.length(); ++i)
    {
        DocumentEntry* entry = timeStampList.at(i);
        QString text = ui->txtDocumentNumberScheme->text().replace(setting.value("NumberingWildcardChar").toString(), QString::number(i + ui->spinBoxNumberingStartsFrom->value()));
        entry->entryNumber->setText(text);
		entry->setFont(font);
    }

    // set document browser button font
    QFont f = setting.value("DocumentBrowserTextSize").value<QFont>();
    f.setPointSize(setting.value("DocumentBrowserTextSize").toInt());
    for(QPushButtonFile* btn : buttons)
	{
		bool bold = btn->font().bold();
        btn->setFont(f);
		btn->setBold(bold);
	}
    ui->txtDocumentBrowserSearch->setFont(f);
	int titleHeight2 = std::max(35, static_cast<int>(QFontMetrics(f).height()) + 5);
    ui->txtDocumentBrowserSearch->setFixedHeight(titleHeight2);
    ui->btnSearchConfig->setFixedSize(titleHeight2, titleHeight2);
    ui->btnNewDocument->setFont(f);
    ui->btnNewDocument->setFixedHeight(titleHeight2);

    // top bar: set visibility and text for each button
	bool b = setting.value("WindowShowTopToolbar").toBool();
	static QMap<QPushButton*, QString> WindowTopToolbarContents = {
		// All buttons of the top bar must be listed here
		{ui->btnSetEpoch, "Set Epoch"},
		{ui->btnTimeStamp, "New L1 topic"},
		{ui->btnTImeStampSmall, "New L2 subtopic"},
		{ui->btnInsertSymbol, "Insert symbol"},
        {ui->btnTimeInsertMode, "Switch to other time mode"},
        {ui->btnEditDocumentFields, "Preferences"},
        {ui->btnSaveDocument, "Save"},
        {ui->btnSearch, "Search this"}
	};
	// dynamically titled buttons: edit map
	WindowTopToolbarContents[ui->btnTimeInsertMode] = (setting.value("TimeInsertModeRelative").toBool() ?
															   "Switch to absolute time mode" :
															   "Switch to relative time mode");
    // NOTE: These lines are necessary for the shortcuts and text to still work.
    ui->btnTimeStamp->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
    ui->btnTImeStampSmall->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_M));
    ui->btnInsertSymbol->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
	// set all buttons, using values from map
	for (auto btn : WindowTopToolbarContents.keys())
	{
		btn->setVisible(b);
        btn->setFlat(!setting.value("ShowButtonText").toBool());
		if(setting.value("ShowButtonText").toBool())
		{
			btn->setText(WindowTopToolbarContents[btn]);
			btn->setToolTip((btn->shortcut().toString().length() > 0 ?
								 "Shortcut: " + btn->shortcut().toString() :
                                 "Shortcut: none"));
		}
		else
		{
			btn->setToolTip((btn->shortcut().toString().length() > 0 ?
								 WindowTopToolbarContents[btn] + "\n" + "Shortcut: " + btn->shortcut().toString() :
								 WindowTopToolbarContents[btn]));
			btn->setText("");
		}
	}
    ui->vLineButtonBar->setVisible(b);
    // NOTE: These lines are necessary for the shortcuts and text to still work.
    ui->btnTimeStamp->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
    ui->btnTImeStampSmall->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_M));
    ui->btnInsertSymbol->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));

	// Document browser widget
	ui->scrollAreaDocumentBrowser->setVisible(setting.value("ShowDocumentBrowserInEditor").toBool());
	ui->actionDocument_browser_widget->setChecked(setting.value("ShowDocumentBrowserInEditor").toBool());
    ui->splitter->setCollapsible(1, setting.value("DocumentBrowserCollapsible").toBool());
    if(setting.value("DocumentBrowserScrollbar").toBool())
        ui->scrollAreaDocumentBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
    else
        ui->scrollAreaDocumentBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    if(!ui->splitter->isCollapsible(1))
        ui->splitter->setSizes({99999999, 1});

	// restore conditions
	ui->textEdit->setTextCursor(cursor);
}

void MainWindow::resetSettingsToDefault()
{
	QSettings setting("Cloud", "TimeStamper");
    setting.setValue("TestValue", "TestValue");
	// No tab
	setting.setValue("TimeInsertModeRelative", true);
	setting.setValue("SplitterPosition", QVariant::fromValue(QList<int>({99999999, 0})));
	// Document tab
	setting.setValue("DefaultDocumentTitle", "Untitled Document");
	setting.setValue("DefaultDocumentSubtitle", "");
    setting.setValue("DefaultDocumentDateStyle", "yyyy.MM.dd hh:mm:ss");
	// Editor tab
    setting.setValue("WindowShowTopToolbar", true);
	setting.setValue("ShowButtonText", true);
    setting.setValue("ShowDocumentBrowserInEditor", true);
	setting.setValue("MainTextSize", 14);
    setting.setValue("MainTextFont", QFont("Cambria"));
    setting.setValue("DocumentBrowserTextSize", 10);
    setting.setValue("DocumentBrowserMaxWidth", 0);
    setting.setValue("NumberingWildcardChar", "%");
	setting.setValue("DocumentBrowserCollapsible", false);
	setting.setValue("EditorDisabled", true);
    setting.setValue("DocumentBrowserScrollbar", true);
	// Storage tab
	setting.setValue("InnerSaveFileLocation", "./");
	// Search tab
    setting.setValue("SearchFlags", 0b1111111111);
    setting.setValue("SearchInstantly", true);
	setting.setValue("SearchOrder", QStringList{"Epoch (Ascending)", "Title (Ascending)"});
	// General tab
	setting.setValue("WindowShowMaximized", true);
}


DocumentEntry* MainWindow::addNewTimeStamp(Document::Entry e, bool redrawUI)
{
    QSettings setting("Cloud", "TimeStamper");

    // Set value in case of default
	if(e.time == QTime(0, 0))
	{
		if(setting.value("TimeInsertModeRelative").toBool())
			e.time = QTime(0, 0).addSecs(static_cast<int>(ui->dateTimeEditEpoch->dateTime().secsTo(QDateTime::currentDateTime())));
		else
			e.time = QTime::currentTime();
	}

    // Create and show entry
    auto* entry = new DocumentEntry(e, ui->txtDocumentNumberScheme->text().replace(setting.value("NumberingWildcardChar").toString(), QString::number(timeStampList.length() + 1)), this);
    ui->vLayoutTextEditor->addLayout(entry);
    timeStampList.push_back(entry);
    // Chain after previous
    int timeStampPosition = timeStampList.length() - 1;
    if(timeStampPosition == 0)
        timeStampList.at(timeStampPosition)->chainAfter(ui->textEdit);
    else
        timeStampList.at(timeStampPosition)->chainAfter(*timeStampList.at(timeStampPosition - 1));
	if(redrawUI)
	{
		loadChangeableSettings();
	}
	return entry;
}

void MainWindow::insertSmallTimeStamp(QTime t)
{
	if(timeStampList.length() == 0)
		return;
    if(t == QTime(0, 0))
    {
        QSettings setting("Cloud", "TimeStamper");
        if(setting.value("TimeInsertModeRelative").toBool())
            t = QTime(0, 0).addSecs(static_cast<int>(ui->dateTimeEditEpoch->dateTime().secsTo(QDateTime::currentDateTime())));
        else
            t = QTime::currentTime();
    }
    timeStampList.last()->appendText(t.toString());
    timeStampList.last()->body->setFocus();
    timeStampList.last()->body->moveCursor(QTextCursor::End);
}

void MainWindow::insertTimeStampImportant()
{
	addNewTimeStamp({QTime(0, 0), "Important!", ""});
}

void MainWindow::insertSymbol(QString str)
{
    auto symbol = mapping.find(str);
    if (str != "")
    {
        if(symbol != mapping.end())
        {
            QWidget* p = QApplication::focusWidget();
            if(dynamic_cast<QLineEdit*>(p))
                dynamic_cast<QLineEdit*>(p)->insert(symbol.value());
            else if(dynamic_cast<QTextEdit*>(p))
                dynamic_cast<QTextEdit*>(p)->insertPlainText(symbol.value());
			else if(lastSelectedEdit == textEdits::line && lastSelectedLineEdit)
				lastSelectedLineEdit->insert(symbol.value());
			else if(lastSelectedEdit == textEdits::text && lastSelectedTextEdit)
				lastSelectedTextEdit->insertPlainText(symbol.value());
        }
        else
            QMessageBox::information(this, "Information", "Couldn't find text in dictionary!");
    }
}

DocumentEntry *MainWindow::insertTimeStampBefore(int timeStampPosition, bool redrawUI)
{
    qDebug() << "Inserting timestamp at position" << timeStampPosition;
    QSettings setting("Cloud", "TimeStamper");
    Document::Entry e;
    auto* entry = new DocumentEntry(e, ui->txtDocumentNumberScheme->text().replace(setting.value("NumberingWildcardChar").toString(),
                                                                                   QString::number(timeStampList.length() + 1)), this);
    ui->vLayoutTextEditor->insertLayout(timeStampPosition + 3, entry);
    timeStampList.insert(timeStampPosition, entry);
    // Chain after previous
    if(timeStampPosition == 0)
        timeStampList.at(timeStampPosition)->chainAfter(ui->textEdit);
    else
        timeStampList.at(timeStampPosition)->chainAfter(*timeStampList.at(timeStampPosition - 1));
    // Chain successor after this
    if(timeStampList.length() > timeStampPosition)
        timeStampList.at(timeStampPosition + 1)->chainAfter(*entry);
    if(redrawUI)
    {
        loadChangeableSettings();
    }
    return entry;
}

void MainWindow::updateLoadableDocuments()
{
    // clear all elements from layout
    QHBoxLayout* item;
    while ( ( item = static_cast<QHBoxLayout*>(ui->vLayoutLoadableDocuments->takeAt( 0 ) )) != nullptr )
    {
        for(QLayoutItem* item2 = item->takeAt(0); item2 != nullptr; item2 = item->takeAt(0))
        {
            delete item2->widget();
            delete item2;
        }
        delete item;
    }
    buttons.clear();

    // Select buttons/entries to be displayed
	QSettings setting("Cloud", "TimeStamper");
	int searchFlags = setting.value("SearchFlags").toInt();
	auto documentToDisplay = fileSystem->getDocuments(ui->txtDocumentBrowserSearch->text(),
																			searchFlags,
																			setting.value("SearchOrder").toStringList());
    Document::KeyType selectedDocument = fileSystem->getOpenDocument() ? fileSystem->getOpenDocument()->key : "";
    int buttonFont = setting.value("DocumentBrowserTextSize").toInt();

    // (re)populate layout
    for(const Document& doc : documentToDisplay) {
        QHBoxLayout* lay = new QHBoxLayout(this);
        ui->vLayoutLoadableDocuments->addLayout(lay);

        // Set up 'Load document' button
        auto* p = new QPushButtonFile(doc.key, "", this);

        // set looks
        QString buttonTextPreview = doc.title + "\n";
        if(doc.subtitle.length() > 0)
            buttonTextPreview += doc.subtitle + "\n";
        buttonTextPreview += QStringRef(&doc.text, 0, std::min(300, doc.text.length()));
        p->setText(doc.title, doc.subtitle, doc.epoch, buttonTextPreview);
        p->setFlat(true);
        p->setStyleSheet("QPushButton {background-color: rgba(255, 255, 255, 15)}");
        p->setBold(doc.key == selectedDocument);
        p->setFontSize(buttonFont);

        // set functionality
        connect(p, SIGNAL(clicked(DocumentManager::KeyType)), this, SLOT(loadDocument(DocumentManager::KeyType)));
		connect(ui->splitter, SIGNAL(splitterMoved(int, int)), p, SLOT(recalculateWidth(int, int)));
        buttons.push_back(p);
        lay->addWidget(p);
        // Without processEvents, the display will be skewed because the button won't know its actual width
        qApp->processEvents();
		p->recalculateWidth(ui->splitter->sizes().at(0), 0);

        // Set up 'Delete document' button
        auto* del = new QPushButtonFile(doc.key, "", this);
        del->setFlat(true);
        del->setStyleSheet("QPushButton {background-color: rgba(255, 255, 255, 15)}");
        connect(del, SIGNAL(clicked(DocumentManager::KeyType)), this, SLOT(deleteDocument(DocumentManager::KeyType)));
        del->setFixedSize(QSize(50, 30));
        del->setIcon(QIcon(":/Images/Icons/trash.svg"));
        del->setToolTip("Delete this document");
        lay->addWidget(del);
    }

}

void MainWindow::updateReadOnlyWidgets()
{
    setWindowTitle(ui->txtTitle->text() + " - Cloud's TimeStamper");
}

void MainWindow::insertFellAsleepTimeStamp()
{
	addNewTimeStamp({QTime(0, 0), "Possible missed notes before this entry!", ""});
}

void MainWindow::hideSearchWidget()
{
    ui->txtSearchSimple->setText("");
    ui->btnSearchNext->hide();
    ui->btnSearchPrev->hide();
    ui->txtSearchSimple->hide();
    ui->btnSearchClose->hide();
    ui->btnSearchThis->hide();
    ui->spinBoxSearchResult->hide();
    if(timeStampList.empty())
        ui->textEdit->setFocus();
    else
		timeStampList.at(timeStampList.size() - 1)->setFocus();
    searchResults = nullptr;
}

void MainWindow::showSearchWidget(bool switchFocus)
{
    ui->btnSearchNext->show();
    ui->btnSearchPrev->show();
    ui->txtSearchSimple->show();
    ui->btnSearchClose->show();
    ui->btnSearchThis->show();
    ui->spinBoxSearchResult->setVisible(false);
    if(switchFocus)
        ui->txtSearchSimple->setFocus();
}

void selectLineEdit(QLineEdit* widget, int pos, int length)
{
	widget->setCursorPosition(pos);
	widget->cursorForward(true, length);
	widget->setFocus();
}

void selectTextEdit(QTextEdit* widget, int pos, int length)
{
	QTextCursor c = widget->textCursor();
	c.setPosition(pos);
	c.setPosition(pos + length, QTextCursor::KeepAnchor);
	widget->setTextCursor(c);
	widget->setFocus();
}

void MainWindow::showSearchResult(int i)
{
	if(!searchResults || searchResults->length() == 0 || i > searchResults->length())
		return;
	const Document::SearchResult& r = searchResults->at(i - 1);
	int length = ui->txtSearchSimple->text().length();
    ui->spinBoxSearchResult->show();
	if(r.entryIndex == -1) // non-entry part
    {
		if(r.whichField == Document::SearchResult::Type::time)
		{
			ui->dateTimeEditEpoch->setFocus();
			ui->dateTimeEditEpoch->setStyleSheet("DateTimeEdit {background:black}");
			return;
		}
		if(r.whichField == Document::SearchResult::Type::body)
		{
			selectTextEdit(ui->textEdit, r.startsFrom, length);
			return;
		}
		if(r.whichField == Document::SearchResult::Type::title)
		{
			selectLineEdit(ui->txtTitle, r.startsFrom, length);
			return;
		}
		if(r.whichField == Document::SearchResult::Type::subtitle)
		{
			selectLineEdit(ui->txtSubtitle, r.startsFrom, length);
			return;
		}
    }
    else if(r.whichField == Document::SearchResult::Type::time)
    {
        timeStampList.at(r.entryIndex)->time->setFocus();
		return;
    }
    else if(r.whichField == Document::SearchResult::Type::title)
    {
		selectLineEdit(timeStampList.at(r.entryIndex)->title, r.startsFrom, length);
    }
    else if(r.whichField == Document::SearchResult::Type::body)
    {
		selectTextEdit(timeStampList.at(r.entryIndex)->body, r.startsFrom, length);
	}
}

void MainWindow::openSettings(const QString &page)
{
	if(settingsWindow != nullptr)
		return;

	QSettings setting("Cloud", "TimeStamper");
	if(setting.value("EditorDisabled").toBool())
		this->setEnabled(false);
	settingsWindow = new SettingsDialog(*this);
	settingsWindow->goToPage(page);
	connect(settingsWindow, &SettingsDialog::settingSaved, this, &MainWindow::loadSettings);
	connect(settingsWindow, &SettingsDialog::closed, this, &MainWindow::reEnableSettings);
	settingsWindow->show();
}

void MainWindow::setSettingsVisibility(bool visible)
{
    ui->lblDocumentSettingsDisplay->setVisible(visible);
    ui->lblDocumentNumberScheme->setVisible(visible);
    ui->txtDocumentNumberScheme->setVisible(visible);
    ui->lblStartFrom->setVisible(visible);
    ui->spinBoxNumberingStartsFrom->setVisible(visible);
    ui->hLineDocumentSettings->setVisible(visible);
    if(visible)
    {
        ui->formLayoutDocumentSettings->setMargin(6);
        ui->btnOpenDocumentOptions->setIcon(QIcon(":/Images/Icons/up-arrow.png"));
    }
    else
    {
        ui->formLayoutDocumentSettings->setMargin(0);
        ui->btnOpenDocumentOptions->setIcon(QIcon(":/Images/Icons/down-arrow.png"));
    }
}

void MainWindow::on_escapePressed()
{
    if(ui->txtSearchSimple->hasFocus())
        hideSearchWidget();
    else if(ui->txtDocumentBrowserSearch->hasFocus())
    {
        ui->textEdit->setFocus();
        QSettings setting("Cloud", "TimeStamper");
        ui->scrollAreaDocumentBrowser->setVisible(setting.value("ShowDocumentBrowserInEditor").toBool());
    }
    // This is a separate branch so that the ones above take priority
    else if(ui->txtSearchSimple->isVisible())
		hideSearchWidget();
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    if(doUnsavedChangesExist())
    {
        if(QMessageBox::question(this, "Exit?", "There are unsaved changes. Do you want to quit?") == QMessageBox::No)
            event->ignore();
        else
            event->accept();
    }
}

void MainWindow::deleteEntry(DocumentEntry *e)
{
    delete e;
	timeStampList.removeOne(e);
    QSettings setting("Cloud", "TimeStamper");
    for(int i = 0; i < timeStampList.length(); ++i)
    {
        QString str = ui->txtDocumentNumberScheme->text();
        timeStampList.at(i)->entryNumber->setText(str.replace(setting.value("NumberingWildcardChar").toString(), QString::number(i + ui->spinBoxNumberingStartsFrom->value())));
    }
}

void MainWindow::loadSettings()
{
	loadChangeableSettings();
    updateLoadableDocuments();
}

void MainWindow::on_btnSetEpoch_clicked()
{
    if(QMessageBox::question(this, "Almost done!", "Are you sure you want to set the epoch manually?\nIt will be set to the time you press OK here!",
									   QMessageBox::Yes|QMessageBox::No, QMessageBox::Yes) == QMessageBox::No)
		return;
    ui->dateTimeEditEpoch->setDateTime(QDateTime::currentDateTime());
    updateReadOnlyWidgets();
	QMessageBox::information(this, "Information", "Epoch set to " + ui->dateTimeEditEpoch->dateTime().toString("hh:mm:ss"));
}

void MainWindow::on_btnTimeStamp_clicked()
{
	addNewTimeStamp();
}

void MainWindow::on_actionExport_to_file_triggered()
{
    // Save
    saveCurrentDocument();
}

void MainWindow::on_actionExport_to_new_file_triggered()
{
    static bool inUse = false;
    if(inUse)
        return;
    inUse = true;

    // Save as Duplicate
    Document::KeyType selectedDocument = "";
    if(fileSystem->getOpenDocument())
    {
         selectedDocument = fileSystem->getOpenDocument()->key;
    }
    fileSystem->setNoOpenDocument();
    fileSystem->storeDocument(makeDocumentFromView(), false);
    if(selectedDocument.length() > 0)
        fileSystem->setOpenDocument(selectedDocument);
    updateLoadableDocuments();
    QMessageBox::information(this, "Duplication successful!", "Duplicate has been stored, but the old one is still selected.");

    inUse = false;
}

void MainWindow::on_actionExit_triggered()
{
	if(doUnsavedChangesExist())
		if(QMessageBox::question(this, "Exit?", "There are unsaved changes. Do you want to quit?") == QMessageBox::No)
			return;
	this->close();
}

void MainWindow::on_btnInsertSymbol_clicked()
{
	// freeze values because QInputDialog messes these up
	auto a = lastSelectedEdit;
	auto* b = lastSelectedLineEdit;
	auto* c = lastSelectedTextEdit;
	QString text = QInputDialog::getText(this, "Insert Symbol", "Name the symbol");
	// restore values before calling insertSymbol
	lastSelectedEdit = a;
	lastSelectedLineEdit = b;
	lastSelectedTextEdit = c;
	insertSymbol(text);
}

void MainWindow::on_btnTimeInsertMode_clicked()
{
    QSettings setting("Cloud", "TimeStamper");
	setting.setValue("TimeInsertModeRelative", !setting.value("TimeInsertModeRelative").toBool());
	loadChangeableSettings();
}

void MainWindow::on_actionPreferences_triggered()
{
	openSettings("document");
}

void MainWindow::on_actionRestoreDefaultDocument_triggered()
{
    if(doUnsavedChangesExist() && QMessageBox::question(this, "Clear working document?", "Do you want to start a new document?\nEverything not saved will be lost!") == QMessageBox::No)
		return;
    createNewDocument();
}

void MainWindow::on_actionExport_to_Markdown_triggered()
{
	exportAsMarkdown();
}

void MainWindow::on_actionAbout_me_triggered()
{
    QMessageBox::information(this, "About Me", "I'm Varga Bence Levente. I hope you have found this tool useful!");
}

void MainWindow::on_actionDocument_browser_widget_triggered(bool checked)
{
	QSettings setting("Cloud", "TimeStamper");
	setting.setValue("ShowDocumentBrowserInEditor", checked);
    loadChangeableSettings();
}

void MainWindow::on_actionSearch_current_document_triggered()
{
	showSearchWidget(true);
}

void MainWindow::on_btnSearchClose_clicked()
{
	hideSearchWidget();
}

void MainWindow::on_txtSearchSimple_returnPressed()
{
	on_btnSearchThis_clicked();
}

void MainWindow::on_btnSearchNext_clicked()
{
    if(!static_cast<bool>(searchResults) || searchResults->length() == 0)
        return;
    ui->spinBoxSearchResult->setVisible(true);
    ui->spinBoxSearchResult->setValue(ui->spinBoxSearchResult->value() % searchResults->length() + 1);
    showSearchResult(ui->spinBoxSearchResult->value());
}

void MainWindow::on_btnSearchPrev_clicked()
{
    if(!static_cast<bool>(searchResults) || searchResults->length() == 0)
        return;
    ui->spinBoxSearchResult->setVisible(true);
    if(ui->spinBoxSearchResult->value() == 1)
        ui->spinBoxSearchResult->setValue(searchResults->length());
    else
        ui->spinBoxSearchResult->setValue(ui->spinBoxSearchResult->value() - 1);
    showSearchResult(ui->spinBoxSearchResult->value());
}

void MainWindow::on_txtSearchSimple_textChanged(const QString&)
{
	ui->spinBoxSearchResult->setVisible(false);
	ui->spinBoxSearchResult->setValue(0);
    searchResults = nullptr;
}

void MainWindow::on_txtDocumentBrowserSearch_textChanged(const QString)
{
    ui->txtDocumentBrowserSearch->blockSignals(true);
    QSettings setting("Cloud", "TimeStamper");
    if(setting.value("SearchInstantly").toBool())
    {
        updateLoadableDocuments();
    }
    ui->txtDocumentBrowserSearch->blockSignals(false);
}

void MainWindow::on_btnSearchConfig_clicked()
{
	openSettings("search");
}

void MainWindow::on_actionSearch_all_documents_triggered()
{
	ui->scrollAreaDocumentBrowser->setVisible(true);
	ui->txtDocumentBrowserSearch->setFocus();
}

void MainWindow::on_textEdit_textChanged()
{
    static int descriptionHeight = 100;
    if(static_cast<int>(ui->textEdit->document()->size().height()) > descriptionHeight)
        ui->textEdit->setFixedHeight(static_cast<int>(ui->textEdit->document()->size().height()));
    else
        ui->textEdit->setFixedHeight(descriptionHeight);
}

void MainWindow::on_btnTImeStampSmall_clicked()
{
    insertSmallTimeStamp();
}

void MainWindow::on_actionHow_to_use_triggered()
{
	if(!QDesktopServices::openUrl(QUrl("file:///" + QCoreApplication::applicationDirPath() + "/TimeStamperHelp.html")))
		QMessageBox::information(this, "Error", "Error opening help.\nPlease manually locate and open " + QUrl(QCoreApplication::applicationDirPath() +  "/TimeStamperHelp.html").toString());
}

void MainWindow::on_actionReset_to_default_settings_triggered()
{
    if(QMessageBox::question(this, "Are you sure?", "Reset every setting to default?") == QMessageBox::Yes)
		resetSettingsToDefault();
}

void MainWindow::on_actionExport_All_triggered()
{
    QSettings setting("Cloud", "TimeStamper");
	QString path = QFileDialog::getExistingDirectory(this, "Choose folder");
    if(path.length() == 0)
        return;
	for(const Document& d : fileSystem->getDocuments())
		fileSystem->writeToFile(d.toMarkdown(), path + "/" + d.title + " " + d.epoch.toString("yyyy_MM_dd_hh_mm_ss") + ".md");
}

void MainWindow::on_btnEditDocumentFields_clicked()
{
	on_actionPreferences_triggered();
}

void MainWindow::on_btnNewDocument_clicked()
{
    on_actionRestoreDefaultDocument_triggered();
}

void MainWindow::on_btnSaveDocument_clicked()
{
	saveCurrentDocument();
}

void MainWindow::on_btnSearch_clicked()
{
    showSearchWidget(true);
}

void MainWindow::on_btnAddTimestampTextEditor_clicked()
{
    on_btnTimeStamp_clicked();
}

void MainWindow::on_btnSearchThis_clicked()
{
    if(ui->txtSearchSimple->text().length() == 0)
        return;
    QSettings setting("Cloud", "TimeStamper");
    searchResults = std::make_unique<QVector<Document::SearchResult>>(makeDocumentFromView().searchDocumentText(
                                                                          ui->txtSearchSimple->text(),
                                                                          setting.value("SearchFlags").toInt()));
    if(searchResults->length() == 0)
    {
        ui->spinBoxSearchResult->hide();
        QMessageBox::information(this, "Search", "Could not find text within the document!");
    }
    else
    {
        ui->spinBoxSearchResult->setRange(1, searchResults->length());
        ui->spinBoxSearchResult->setValue(1);
        showSearchResult(1);
    }
}

void MainWindow::on_actionExport_as_PlainText_triggered()
{
    exportAsPlainText();
}

void MainWindow::on_spinBoxSearchResult_editingFinished()
{
    showSearchResult(ui->spinBoxSearchResult->value());
}

void MainWindow::on_btnOpenDocumentOptions_clicked()
{
    setSettingsVisibility(!ui->lblDocumentSettingsDisplay->isVisible());
}

void MainWindow::on_txtDocumentNumberScheme_textChanged(const QString &arg1)
{
    QSettings setting("Cloud", "TimeStamper");
    for(int i = 0; i < timeStampList.length(); ++i)
    {
        QString str = arg1;
        timeStampList.at(i)->entryNumber->setText(str.replace(setting.value("NumberingWildcardChar").toString(), QString::number(i + ui->spinBoxNumberingStartsFrom->value())));
    }
}

void MainWindow::on_spinBoxNumberingStartsFrom_valueChanged(int)
{
    on_txtDocumentNumberScheme_textChanged(ui->txtDocumentNumberScheme->text());
}

void MainWindow::on_actionCredits_triggered()
{
    QString str = "";
	str += "The program is made with the Qt framework.\n";
	str += "Icons made by Roundicons, Those Icons, mavadee, Good Ware, Freepik from www.flaticon.com.";
    QMessageBox::information(this, "Credits", str, QMessageBox::Close);
}

void MainWindow::on_actionDelete_all_documents_triggered()
{
    if(QMessageBox::question(this, "Are you sure?", "Are you really sure that you want to delete all documents?") == QMessageBox::No)
        return;
    if(QMessageBox::question(this, "Are you sure?", "Are you REALLY sure that you want to delete all documents?") == QMessageBox::No)
        return;
    if(fileSystem->deleteAllDocuments())
        QMessageBox::information(this, "Success", "Documents successfully cleared!");
    else
    {
        QString str = "Something went wrong during deletion. Please, check your files.\n";
        str += "You can also delete the TimeStamperDocuments.db file if the error persists.";
        QMessageBox::information(this, "Error", str);
    }
    updateLoadableDocuments();
}

void MainWindow::on_actionCreate_backup_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Select backup location", "./TimeStamperDocumentsBackup.db");
    if(path.length() > 0)
    {
        if(fileSystem->createBackup(path))
            QMessageBox::information(this, "Success", "Backup successfully created!");
        else
            QMessageBox::information(this, "Error", "Something went wrong. Please, copy the file TimeStamperDocuments.db manually.");
    }
}

void MainWindow::on_actionRestore_backup_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Select backup location", "./TimeStamperDocumentsBackup.db");
    if(path.length() > 0 &&
            QMessageBox::question(this, "Are you sure?", "Do you really want to overwrite all your current documents?") == QMessageBox::Yes)
    {
        try
        {
            QSettings setting("Cloud", "TimeStamper");
            if(QFileInfo (path) == QFileInfo(QDir::toNativeSeparators(setting.value("InnerSaveFileLocation").toString() + "/TimeStamperDocuments.db")))
            {
                QMessageBox::information(this, "Error", "You can't import the current working database!");
                return;
            }
            fileSystem = nullptr;
            QFile(QDir::toNativeSeparators(setting.value("InnerSaveFileLocation").toString() + "/TimeStamperDocuments.db")).remove();
            bool error = !QFile(path).copy(QDir::toNativeSeparators(setting.value("InnerSaveFileLocation").toString() + "/TimeStamperDocuments.db"));
            if(error)
                QMessageBox::warning(this, "Error", "Can't copy file to import backup.");
            fileSystem = std::make_unique<DocumentManager>(QDir::toNativeSeparators(setting.value("InnerSaveFileLocation").toString()));
            if(error)
                return;
            QMessageBox::information(this, "Success", "Backup successfully imported!");
            updateLoadableDocuments();
        }
        catch(std::exception& e)
        {
            QMessageBox::information(this, "Error", "Something went wrong. Please, copy the file TimeStamperDocuments.db manually. Error: "
                                     + QString::fromStdString(e.what()));
        }
    }
}

void MainWindow::on_txtDocumentBrowserSearch_returnPressed()
{
    QSettings setting("Cloud", "TimeStamper");
    if(!setting.value("SearchInstantly").toBool())
        updateLoadableDocuments();
}

void MainWindow::on_splitter_splitterMoved(int, int)
{
    QSettings setting("Cloud", "TimeStamper"); // According to Qt Documentation, this is constructed really quickly
    setting.setValue("SplitterPosition", QVariant::fromValue(ui->splitter->sizes()));
}

void MainWindow::on_actionInsert_timestamp_before_entry_triggered()
{
    if(timeStampList.length() == 0)
        return;
    bool ok;
    int pos = QInputDialog::getInt(this, "Which entry?", "Entry number", ui->spinBoxNumberingStartsFrom->value(),
                         ui->spinBoxNumberingStartsFrom->value(), ui->spinBoxNumberingStartsFrom->value() + timeStampList.length() - 1, 1, &ok);
    if(ok)
        insertTimeStampBefore(pos - ui->spinBoxNumberingStartsFrom->value());
}
