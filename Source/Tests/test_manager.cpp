#include <QtTest>

#include "test_manager.h"

#include "../App/documentmanager.h"

#include "config.h"

Q_DECLARE_METATYPE(Document::Entry);

const QString ManagerTest::path = "/TIMESTAMPERTESTS/";

void ManagerTest::initTestCase()
{
    QDir(path).removeRecursively();
}

void ManagerTest::cleanupTestCase()
{
	QDir(path).removeRecursively();
}

void ManagerTest::init()
{
	QDir(path).removeRecursively();
}

void ManagerTest::dbIsUsableAfterGoodConstruction()
{
    QFETCH(QString, path);

    // no exceptions should be thrown
    DocumentManager db(path);

    // Check that these also don't throw on usage
    db.getDocuments();
    db.getDocument("");
    db.documentWithKeyExists("");
    db.getOpenDocument();
    db.getPath();
}

void ManagerTest::dbIsUsableAfterGoodConstruction_data()
{
    QTest::addColumn<QString>("path");
    QTest::newRow("current") << QDir::currentPath();
    QTest::newRow("currentSubdir1") << (QDir::currentPath() + "/TestDir1/");
    QTest::newRow("currentSubdir2") << (QDir::currentPath() + "/Test Dir 2/Muhaha/");
    // These will be created under C:/...
    QTest::newRow("no root 1") << "TestDir1/";
    QTest::newRow("no root 2") << "Test Dir 2/Muhaha/";
}

void ManagerTest::dbThrowsAfterBadConstruction()
{
    QFETCH(QString, path);
    QVERIFY_EXCEPTION_THROWN(DocumentManager db(path), std::exception);
}

void ManagerTest::dbThrowsAfterBadConstruction_data()
{
    QTest::addColumn<QString>("path");
    QTest::newRow("empty") << "";
    QTest::newRow("Unwritable") << "C:/Windows/TIMESTAMPER_TEST_THIS_SHOULDNT_EXIST";
}

void ManagerTest::dbGetDocumentsEmpty()
{
    DocumentManager db(path);
    QCOMPARE(db.getDocuments().length(), 0);
    QCOMPARE(db.getOpenDocument(), nullptr);
    QCOMPARE(db.getDocument(""), nullptr);
    QCOMPARE(db.getDocument("TestKey"), nullptr);
    QCOMPARE(db.getDocument("1"), nullptr);
    QCOMPARE(db.getDocument("2"), nullptr);
    QCOMPARE(db.getDocument("0"), nullptr);
}

void ManagerTest::dbStoreDocumentIntoEmptyDatabase()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);

    Document doc(title, subtitle, text, epoch);
    doc.entries = entries;

    // Because Qt's cleanup test case doesn't seem to work with their data driven tools... *sigh*
    QDir(path).removeRecursively();
    DocumentManager db1(path);

	QCOMPARE(db1.getDocument("0"), nullptr);
	QCOMPARE(db1.getDocuments().length(), 0);
    QVERIFY(db1.storeDocument(doc));
    QCOMPARE(db1.getDocuments().length(), 1);
    Document doc1 = db1.getDocuments().at(0);
    QVERIFY(doc.equals(doc1));
    Document::KeyType key = db1.getDocuments().at(0).key;
    QVERIFY(db1.getDocument(key) != nullptr);
    Document d = *db1.getDocument(key);
    QVERIFY(d.equals(doc));
}

void ManagerTest::dbStoreDocumentIntoEmptyDatabase_data()
{
    ALL_TEST_DOCUMENTS
}

void ManagerTest::dbStoreSameDocumentMultipleTimes()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);

    Document doc(title, subtitle, text, epoch);
    doc.entries = entries;

    QDir(path).removeRecursively();
    DocumentManager db1(path);

	QCOMPARE(db1.getDocument("0"), nullptr);
	QCOMPARE(db1.getDocuments().length(), 0);

    for(int i = 0; i < 100; ++i)
        QVERIFY(db1.storeDocument(doc, true));

    QCOMPARE(db1.getDocuments().length(), 1);
    Document doc1 = db1.getDocuments().at(0);
    QVERIFY(doc.equals(doc1));
    Document::KeyType key = db1.getDocuments().at(0).key;
    QVERIFY(db1.getDocument(key) != nullptr);
    Document d = *db1.getDocument(key);
    QVERIFY(d.equals(doc));
}

void ManagerTest::dbStoreSameDocumentMultipleTimes_data()
{
	ALL_TEST_DOCUMENTS
}

void ManagerTest::dbStoreDocumentOpenDocumentReturnsExpectedResult()
{
	Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));
	DocumentManager db(path);

	QCOMPARE(db.getDocuments().length(), 0);
	QVERIFY(db.storeDocument(d1, true));
	QVERIFY(db.getOpenDocument());
	qDebug() << db.getOpenDocument()->toPlainText();
	QVERIFY(db.getOpenDocument()->equals(d1));
}

void ManagerTest::dbStoreDocumentOverwritesOpenDocument()
{
	Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));
	Document d2("", "", "", QDateTime::fromSecsSinceEpoch(123123231));
	Document d3("Á÷¤ŐÓ \t asd", "ˇ˘˘˛˛ˇ$ß", "1598÷Đ]ä>", QDateTime::fromSecsSinceEpoch(1));

	DocumentManager db(path);
	QCOMPARE(db.getDocuments().length(), 0);
	QVERIFY(db.storeDocument(d1, true));
	QCOMPARE(db.getDocuments().length(), 1);
	QVERIFY(db.getOpenDocument()->equals(d1));
	// overwrites document
	QVERIFY(db.storeDocument(d2, true));
	QCOMPARE(db.getDocuments().length(), 1);
    QVERIFY(db.getOpenDocument()->equals(d2));
    // overwrite again
    QVERIFY(db.storeDocument(d3, true));
    QCOMPARE(db.getDocuments().length(), 1);
    QVERIFY(db.getOpenDocument()->equals(d3));
}

void ManagerTest::dbStoreDocumentMultipleDocuments()
{
	Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));
	Document d2("", "", "", QDateTime::fromSecsSinceEpoch(123123231));
	Document d3("Á÷¤ŐÓ \t asd", "ˇ˘˘˛˛ˇ$ß", "1598÷Đ]ä>", QDateTime::fromSecsSinceEpoch(1));

	DocumentManager db(path);
	QCOMPARE(db.getDocuments().length(), 0);
	QVERIFY(db.storeDocument(d1, true));
	QCOMPARE(db.getDocuments().length(), 1);
	QVERIFY(db.getOpenDocument()->equals(d1));

	db.setNoOpenDocument();

	QVERIFY(db.storeDocument(d2, true));
	QCOMPARE(db.getDocuments().length(), 2);
	QVERIFY(db.getOpenDocument()->equals(d2));

	db.setNoOpenDocument();

	QVERIFY(db.storeDocument(d3, true));
	QCOMPARE(db.getDocuments().length(), 3);
    QVERIFY(db.getOpenDocument()->equals(d3));
}

void ManagerTest::dbInsertAndDeleteOneOpenDocument()
{
    Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));

    DocumentManager db(path);
    QCOMPARE(db.getDocuments().length(), 0);
    QVERIFY(db.storeDocument(d1, true));
    QCOMPARE(db.getDocuments().length(), 1);

    db.deleteDocument(db.getOpenDocument()->key);
    QVERIFY(db.getDocuments().length() == 0);
}

void ManagerTest::dbInsertAndDeleteOneNotOpenDocument()
{
    Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));

    DocumentManager db(path);
    QCOMPARE(db.getDocuments().length(), 0);
    QVERIFY(db.storeDocument(d1, true));
    QCOMPARE(db.getDocuments().length(), 1);

    auto key = db.getOpenDocument()->key;
    db.setNoOpenDocument();

    db.deleteDocument(key);
    QVERIFY(db.getDocuments().length() == 0);
}

void ManagerTest::dbInsertAndDeleteMultipleDocuments()
{
    Document d1("Document 1", "Random subtitle", "Description comes here", QDateTime::fromSecsSinceEpoch(123123));
    Document d2("", "", "", QDateTime::fromSecsSinceEpoch(123123231));
    Document d3("Á÷¤ŐÓ \t asd", "ˇ˘˘˛˛ˇ$ß", "1598÷Đ]ä>", QDateTime::fromSecsSinceEpoch(1));

    DocumentManager db(path);
    QCOMPARE(db.getDocuments().length(), 0);
    QVERIFY(db.storeDocument(d1, false));
    QVERIFY(db.storeDocument(d2, true));
    auto key = db.getOpenDocument()->key;
    db.setNoOpenDocument();
    QVERIFY(db.storeDocument(d3, true));
    QCOMPARE(db.getDocuments().length(), 3);

    QVERIFY(db.getOpenDocument() != nullptr);

    db.deleteDocument(db.getOpenDocument()->key);
    QVERIFY(db.getDocuments().length() == 2);
    QVERIFY(db.getDocument(key) != nullptr);
    QVERIFY(db.getOpenDocument() == nullptr);

    db.deleteDocument(key);
    QVERIFY(db.getDocuments().length() == 1);
    QVERIFY(db.getDocument(key) == nullptr);
    QVERIFY(db.getOpenDocument() == nullptr);
}
