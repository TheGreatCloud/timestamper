#ifndef TEST_MANAGER_H
#define TEST_MANAGER_H

#include <QObject>

class ManagerTest : public QObject
{
    Q_OBJECT

    static const QString path;

private slots:
    void initTestCase();
    void cleanupTestCase();
	void init();

    void dbIsUsableAfterGoodConstruction();
    void dbIsUsableAfterGoodConstruction_data();
    void dbThrowsAfterBadConstruction();
    void dbThrowsAfterBadConstruction_data();
    void dbGetDocumentsEmpty();
    void dbStoreDocumentIntoEmptyDatabase();
    void dbStoreDocumentIntoEmptyDatabase_data();
    void dbStoreSameDocumentMultipleTimes();
    void dbStoreSameDocumentMultipleTimes_data();
	void dbStoreDocumentOpenDocumentReturnsExpectedResult();
	void dbStoreDocumentOverwritesOpenDocument();
    void dbStoreDocumentMultipleDocuments();
    void dbInsertAndDeleteOneOpenDocument();
    void dbInsertAndDeleteOneNotOpenDocument();
    void dbInsertAndDeleteMultipleDocuments();
};

#endif // TEST_MANAGER_H
