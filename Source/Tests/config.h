#ifndef CONFIG_H
#define CONFIG_H

#define ALL_TEST_DOCUMENTS \
    QTest::addColumn<QString>("title"); \
    QTest::addColumn<QString>("subtitle"); \
    QTest::addColumn<QString>("text"); \
    QTest::addColumn<QDateTime>("epoch"); \
    QTest::addColumn<QVector<Document::Entry>>("entries"); \
    \
    QTest::newRow("empty") << "" << "" << "" << QDateTime::fromSecsSinceEpoch(0) << QVector<Document::Entry>{};\
    QTest::newRow("title1") << "Test Document" << "" << "" << QDateTime::fromSecsSinceEpoch(123) << QVector<Document::Entry>{}; \
    QTest::newRow("title2") << "Fordítóprogramok előadás 3." << "" << "" << QDateTime::fromSecsSinceEpoch(122220) << QVector<Document::Entry>{}; \
    QTest::newRow("title chars") << ":<3Ł$[]{}@<>#@{ĐRT@Ä|\\&ÄT" << "" << "" << QDateTime::fromSecsSinceEpoch(50) << QVector<Document::Entry>{}; \
    QTest::newRow("subtitle only") << "" << "Not a test document" << "" << QDateTime::fromSecsSinceEpoch(666) << QVector<Document::Entry>{}; \
    QTest::newRow("title and subtitle") << "ŰáÉpŐúö9ÓÜÖ78¨;ß¨;¤¨¸§asd" << "¤P÷ßO÷Ojwefgznk liuh $$$$$" << "" << QDateTime::fromSecsSinceEpoch(555) << QVector<Document::Entry>{}; \
    QTest::newRow("description") << "" << "" << "Leírás arról hogy minek\n kellett volna.\ntörténnie?:_" << QDateTime::fromSecsSinceEpoch(187368127) << QVector<Document::Entry>{}; \
    QTest::newRow("one empty entry") << "" << "" << "" << QDateTime::fromSecsSinceEpoch(187923) << QVector<Document::Entry>{{QTime(0, 0), "", ""}};

// If defined, longer, generated tests will run instead of smaller patch of tests
//#define FULL_TEST

#endif // CONFIG_H
