#include "test_basic.h"
#include "test_manager.h"

#include <QtTest>

int main(int argc, char** argv)
{
   int status = 0;
   {
       BasicTest tc;
       status |= QTest::qExec(&tc, argc, argv);
   }
   {
       ManagerTest tc;
       status |= QTest::qExec(&tc, argc, argv);
   }
   return status;
}
