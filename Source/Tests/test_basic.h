#ifndef TEST_BASIC_H
#define TEST_BASIC_H

#include <QObject>

class BasicTest : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();

    // constructor
    void documentConstructionDoesNotThrow();
    void documentConstructionDoesNotThrow_data();

    // to plain text
    void documentToPlainTextContainsInfo();
    void documentToPlainTextContainsInfo_data();

    // to markdown
    void toMarkdownContainsInfo();
    void toMarkdownContainsInfo_data();

    // contains text
    void containsTextFindsEmptyString();
    void containsTextFindsEmptyString_data();

    void containsTextFindsTitle();
    void containsTextFindsTitle_data();

    void containsTextFindsSubtitle();
    void containsTextFindsSubtitle_data();

    void containsTextFindsBody();
    void containsTextFindsBody_data();

    void containsTextFindsEpoch();
    void containsTextFindsEpoch_data();

    void containsTextFindsEntry();
    void containsTextFindsEntry_data();

    void containsTextNoResults();
    void containsTextNoResults_data();

    // searchDocumentText
    void searchDocumentTextFindsTitle();
    void searchDocumentTextFindsTitle_data();

    void searchDocumentTextFindsSubtitle();
    void searchDocumentTextFindsSubtitle_data();

    void searchDocumentTextFindsBody();
    void searchDocumentTextFindsBody_data();

    void searchDocumentTextFindsEpoch();
    void searchDocumentTextFindsEpoch_data();

    void searchDocumentTextFindsEntry();
    void searchDocumentTextFindsEntry_data();

    void searchDocumentTextNoResults();
    void searchDocumentTextNoResults_data();

    // comparison
    void equalsReturnsTrue();
    void equalsReturnsFalse();
    void isLessThanWorks();

    // assignment
    void operatorEquals();
    void makeEqualToWorks();

};

#endif // TEST_BASIC_H
