#include <QtTest>
#include <QDir>

#include "test_basic.h"

#include "../App/document.h"
#include "../App/documentmanager.h"

#include "config.h"

Q_DECLARE_METATYPE(Document::Entry);

void BasicTest::initTestCase() {}

void BasicTest::cleanupTestCase() {}

void BasicTest::documentConstructionDoesNotThrow()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);

    // Doc is just a container struct
    Document doc(title, subtitle, text, epoch);
    doc.entries = entries;

    QCOMPARE(doc.title, title);
    QCOMPARE(doc.subtitle, subtitle);
    QCOMPARE(doc.text, text);
    QCOMPARE(doc.epoch, epoch);
    QCOMPARE(doc.entries.length(), entries.length());
    for(int i = 0; i < entries.length(); ++i)
    {
        QCOMPARE(entries.at(i).time, doc.entries.at(i).time);
        QCOMPARE(entries.at(i).title, doc.entries.at(i).title);
        QCOMPARE(entries.at(i).body, doc.entries.at(i).body);
    }
}

void BasicTest::documentConstructionDoesNotThrow_data()
{
    ALL_TEST_DOCUMENTS
}

void BasicTest::documentToPlainTextContainsInfo()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;
    QString str = doc.toPlainText();

    QVERIFY(str.contains(title));
    QVERIFY(str.contains(subtitle));
    QVERIFY(str.contains(text));
    for(auto entry : doc.entries)
    {
        QVERIFY(str.contains(entry.title));
        QVERIFY(str.contains(entry.body));
    }
}

void BasicTest::documentToPlainTextContainsInfo_data()
{
    ALL_TEST_DOCUMENTS
}

void BasicTest::toMarkdownContainsInfo()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;
    QString str = doc.toMarkdown();

    QVERIFY(str.contains(title));
    QVERIFY(str.contains(subtitle));
    QVERIFY(str.contains(text));
    for(auto entry : doc.entries)
    {
        QVERIFY(str.contains(entry.title));
        QVERIFY(str.contains(entry.body));
    }
}

void BasicTest::toMarkdownContainsInfo_data()
{
    ALL_TEST_DOCUMENTS
}

void BasicTest::containsTextFindsEmptyString()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::title)));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::subtitle)));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::epoch)));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::text)));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::entries)));
}

void BasicTest::containsTextFindsEmptyString_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

    QTest::newRow("empty document empty string") << "" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "";
    QTest::newRow("non empty title empty string") << "" << "" << "aksjfgu auh szia ÉÁŰ $ß¤÷P¨˝˝~ˇ^" << QDateTime() << QVector<Document::Entry>{} << "";
}

void BasicTest::containsTextFindsTitle()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::title)));
}

void BasicTest::containsTextFindsTitle_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

#ifdef FULL_TEST
    QVector<std::pair<QString, QString>> answers = {
        {"", ""},
        {"Test Document", ""},
        {"Test Document", "Test"},
        {"Test Document", "Document"},
        {"Test Document", "doc"},
        {"Test Document", "st Doc"},
        {"Test Document", "ent"},
        {"Test Document", "t"},
        {"Test Document", "T"},
        {"Test Document", " "},
        {"\t\t Hello", "\t "},
        {"Helló \t\t Hello", "\t "},
    };
    QStringList strings = {"", "szia", "ßP ¨123", "\t  Teszt strng", "€<>[]{}"};
    QVector<QDateTime> dateTimes = {QDateTime(), QDateTime::currentDateTime(), QDateTime::fromSecsSinceEpoch(123431)};
    QVector<QTime> times = {QTime(), QTime::currentTime(), QTime::fromMSecsSinceStartOfDay(1234)};
    int count = 0;
    for(auto pair : answers)
    {
        const QString& title = pair.first;
        const QString& filter = pair.second;
        for (auto subtitle : strings)
            for (auto desciption : strings)
                for(auto epoch : dateTimes)
                    for(auto entryTime : times)
                        for(auto entryTitle : strings)
                            for(auto entryText : strings)
                                for (int i = 0; i < 3; ++i)
                                {
                                    count++;
                                    QVector<Document::Entry> entr{};
                                    for(int entryCount = 0; entryCount < i; ++entryCount)
                                        entr.push_back({entryTime, entryTitle, entryText});
                                    QTest::newRow(std::to_string(count).data()) << title << subtitle << desciption << epoch << entr << filter;
                                }
    }
#else
    QTest::newRow("title1") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title2") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title3") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title4") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << " ";
    QTest::newRow("title5") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title6") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "Document";
    QTest::newRow("title7") << "ÉÁŰÚŐP" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "ű";
    QTest::newRow("title8") << "ÉÁŰÚŐP" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "űúŐp";
    QTest::newRow("title9") << "asd JKLÉU ÷O ß asf" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "ß";
    QTest::newRow("title10") << "asd JKLÉU ÷O ß asf" << "" << "" << QDateTime() << QVector<Document::Entry>{} << " ß as";
    QTest::newRow("title11") << "Test Document" << "ioauwcgmoiu" << "Random Description" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title12") << "Test Document" << "ÉÁŰ" << " \t  " << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title13") << "Test Document" << "" << "$ß¤÷Pasd " << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title14") << "Test Document" << "163478" << "Test Document" << QDateTime::fromSecsSinceEpoch(876231876125789) << QVector<Document::Entry>{} << "doc";
    QTest::newRow("title15") << "Test Document" << "" << "5342 asdf" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title16") << "Test Document" << "" << "\t\n\n\nDocument" << QDateTime() << QVector<Document::Entry>{} << "Document";
#endif
}

void BasicTest::containsTextFindsSubtitle()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::subtitle)));
}

void BasicTest::containsTextFindsSubtitle_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

#ifdef FULL_TEST
    QVector<std::pair<QString, QString>> answers = {
        {"", ""},
        {"Test Document", ""},
        {"Test Document", "Test"},
        {"Test Document", "Document"},
        {"Test Document", "doc"},
        {"Test Document", "st Doc"},
        {"Test Document", "ent"},
        {"Test Document", "t"},
        {"Test Document", "T"},
        {"Test Document", " "},
        {"\t\t Hello", "\t "},
        {"Helló \t\t Hello", "\t "},
    };
    QStringList strings = {"", "szia", "ßP ¨123", "\t  Teszt strng", "€<>[]{}"};
    QVector<QDateTime> dateTimes = {QDateTime(), QDateTime::currentDateTime(), QDateTime::fromSecsSinceEpoch(123431)};
    QVector<QTime> times = {QTime(), QTime::currentTime(), QTime::fromMSecsSinceStartOfDay(1234)};
    int count = 0;
    for(auto pair : answers)
    {
        const QString& subtitle = pair.first;
        const QString& filter = pair.second;
        for (auto title : strings)
            for (auto desciption : strings)
                for(auto epoch : dateTimes)
                    for(auto entryTime : times)
                        for(auto entryTitle : strings)
                            for(auto entryText : strings)
                                for (int i = 0; i < 3; ++i)
                                {
                                    count++;
                                    QVector<Document::Entry> entr{};
                                    for(int entryCount = 0; entryCount < i; ++entryCount)
                                        entr.push_back({entryTime, entryTitle, entryText});
                                    QTest::newRow(std::to_string(count).data()) << title << subtitle << desciption << epoch << entr << filter;
                                }
    }
#else
    QTest::newRow("title1") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title2") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title3") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title4") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << " ";
    QTest::newRow("title5") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title6") << "" << "Test Document" << "" << QDateTime() << QVector<Document::Entry>{} << "Document";
    QTest::newRow("title7") << "" << "ÉÁŰÚŐP" << "" << QDateTime() << QVector<Document::Entry>{} << "ű";
    QTest::newRow("title8") << "" << "ÉÁŰÚŐP" << "" << QDateTime() << QVector<Document::Entry>{} << "űúŐp";
    QTest::newRow("title9") << "" << "asd JKLÉU ÷O ß asf" << "" << QDateTime() << QVector<Document::Entry>{} << "ß";
    QTest::newRow("title10") << "" << "asd JKLÉU ÷O ß asf" << "" << QDateTime() << QVector<Document::Entry>{} << " ß as";
    QTest::newRow("title11") << "ioauwcgmoiu" << "Test Document" << "Random Description" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title12") << "ÉÁŰ" << "Test Document" << " \t  " << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title13") << "" << "Test Document" << "$ß¤÷Pasd " << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title14") << "163478" << "Test Document" << "Test Document" << QDateTime::fromSecsSinceEpoch(876231876125789) << QVector<Document::Entry>{} << "doc";
    QTest::newRow("title15") << "" << "Test Document" << "5342 asdf" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title16") << "" << "Test Document" << "\t\n\n\nDocument" << QDateTime() << QVector<Document::Entry>{} << "Document";
#endif
}

void BasicTest::containsTextFindsBody()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::text)));
}

void BasicTest::containsTextFindsBody_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

    QTest::newRow("title1") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title2") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title3") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title4") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << " ";
    QTest::newRow("title5") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title6") << "" << "" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Document";
    QTest::newRow("title7") << "" << "" << "ÉÁŰÚŐP" << QDateTime() << QVector<Document::Entry>{} << "ű";
    QTest::newRow("title8") << "" << "" << "ÉÁŰÚŐP" << QDateTime() << QVector<Document::Entry>{} << "űúŐp";
    QTest::newRow("title9") << "" << "" << "asd JKLÉU ÷O ß asf" << QDateTime() << QVector<Document::Entry>{} << "ß";
    QTest::newRow("title10") << "" << "" << "asd JKLÉU ÷O ß asf" << QDateTime() << QVector<Document::Entry>{} << " ß as";
    QTest::newRow("title11") << "ioauwcgmoiu" << "Random Description" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Test";
    QTest::newRow("title12") << "ÉÁŰ" << " \t  " << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Test docum";
    QTest::newRow("title13") << "" << "$ß¤÷Pasd " << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "st docUM";
    QTest::newRow("title14") << "163478" << "Test Document" << "Test Document"  << QDateTime::fromSecsSinceEpoch(876231876125789) << QVector<Document::Entry>{} << "doc";
    QTest::newRow("title15") << "" << "5342 asdf" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << " Document";
    QTest::newRow("title16") << "" << "\t\n\n\nDocument" << "Test Document" << QDateTime() << QVector<Document::Entry>{} << "Document";
}

void BasicTest::containsTextFindsEpoch()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::epoch)));
}

void BasicTest::containsTextFindsEpoch_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

    QString d = "yyyy MM dd hh mm ss";

    QTest::newRow("date1") << "" << "" << "" << QDateTime::fromString("2020 01 01 01 01 01", d) << QVector<Document::Entry>{} << "2020";
    QTest::newRow("date2") << "" << "" << "" << QDateTime::fromString("2020 05 01 01 01 01", d) << QVector<Document::Entry>{} << "may";
    QTest::newRow("date3") << "" << "" << "" << QDateTime::fromString("2020 05 03 01 01 01", d) << QVector<Document::Entry>{} << "3";
    QTest::newRow("date4") << "" << "" << "" << QDateTime::fromString("2020 05 01 01 01 01", d) << QVector<Document::Entry>{} << "01";
    QTest::newRow("date5") << "" << "" << "" << QDateTime::fromString("2020 05 01 11 01 01", d) << QVector<Document::Entry>{} << "11";
    QTest::newRow("date6") << "" << "" << "" << QDateTime::fromString("2020 05 01 11 53 01", d) << QVector<Document::Entry>{} << "53";
    QTest::newRow("date7") << "" << "" << "" << QDateTime::fromString("2020 05 01 11 53 00", d) << QVector<Document::Entry>{} << "0";
}

void BasicTest::containsTextFindsEntry()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.containsText(string, 0));
    QVERIFY(doc.containsText(string, static_cast<int>(Document::searchResultType::entries)));
}

void BasicTest::containsTextFindsEntry_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

    QString d = "hh mm ss";

    QVector<Document::Entry> e {};
    e.push_back({QTime::fromMSecsSinceStartOfDay(0), "", ""});
    QTest::newRow("date1") << "" << "" << "" << QDateTime() << e << "0";
    e.push_back({QTime::fromString("00 11 22", d), "Test Title", "Test Description"});
    QTest::newRow("date2") << "" << "" << "" << QDateTime() << e << "11";
    QTest::newRow("date3") << "" << "" << "" << QDateTime() << e << "Test";
    QTest::newRow("date4") << "" << "" << "" << QDateTime() << e << " Title";
    QTest::newRow("date5") << "" << "" << "" << QDateTime() << e << "dEsCripTion";
    QTest::newRow("date6") << "" << "" << "" << QDateTime() << e << "22";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "0";
    e.push_back({QTime::fromString("16 54 33", d), "äßáű", "\t\t\nTeszt.\n"});
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "16";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "54";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "33";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "ßá";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "\t";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "Teszt";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "\nTeszt";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "eszt.";
    QTest::newRow("date7") << "" << "" << "" << QDateTime() << e << "\t\t\nTeszt.\n";
}

void BasicTest::containsTextNoResults()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(!doc.containsText(string, 0));
    QVERIFY(!doc.containsText(string, static_cast<int>(Document::searchResultType::title)));
    QVERIFY(!doc.containsText(string, static_cast<int>(Document::searchResultType::epoch)));
    QVERIFY(!doc.containsText(string, static_cast<int>(Document::searchResultType::subtitle)));
    QVERIFY(!doc.containsText(string, static_cast<int>(Document::searchResultType::text)));
    QVERIFY(!doc.containsText(string, static_cast<int>(Document::searchResultType::entries)));
}

void BasicTest::containsTextNoResults_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("subtitle");
    QTest::addColumn<QString>("text");
    QTest::addColumn<QDateTime>("epoch");
    QTest::addColumn<QVector<Document::Entry>>("entries");
    QTest::addColumn<QString>("string");

    QTest::newRow("empty document empty string") << "" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "empty";
    QTest::newRow("non empty title empty string") << "aksjfgu auh szia ÉÁŰ $ß¤÷P¨˝˝~ˇ^" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "teszt";
    QTest::newRow("title1") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "Test2";
    QTest::newRow("title2") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "   ";
    QTest::newRow("title3") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "document ";
    QTest::newRow("title4") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "123";
    QTest::newRow("title5") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "\n";
    QTest::newRow("title6") << "Test Document" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "\t";
    QTest::newRow("title7") << "ÉÁŰÚŐP" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "u";
    QTest::newRow("title8") << "ÉÁŰÚŐP" << "" << "" << QDateTime() << QVector<Document::Entry>{} << "Ö";
    QTest::newRow("title9") << "asd JKLÉU ÷O ß asf" << "" << "" << QDateTime() << QVector<Document::Entry>{} << ")";
    QTest::newRow("title10") << "asd JKLÉU ÷O ß asf" << "" << "" << QDateTime() << QVector<Document::Entry>{} << " asd";
    QTest::newRow("title11") << "Test Document" << "ioauwcgmoiu" << "Random Description" << QDateTime() << QVector<Document::Entry>{} << "2030";
    QTest::newRow("title12") << "Test Document" << "ÉÁŰ" << " \t  " << QDateTime() << QVector<Document::Entry>{} << "\n";
    QTest::newRow("title13") << "Test Document" << "" << "$ß¤÷Pasd " << QDateTime() << QVector<Document::Entry>{} << "ent\n";
    QTest::newRow("title14") << "Test Document" << "163478" << "Test Document" << QDateTime::fromSecsSinceEpoch(876231876125789) << QVector<Document::Entry>{} << "9999";
    QTest::newRow("title15") << "Test Document" << "" << "5342 asdf" << QDateTime() << QVector<Document::Entry>{} << "Szia";
    QTest::newRow("title16") << "Test Document" << "" << "\t\n\n\nDocument" << QDateTime() << QVector<Document::Entry>{} << "\tD";
}

void BasicTest::searchDocumentTextFindsTitle()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() > 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::title)).length() > 0);
}

void BasicTest::searchDocumentTextFindsTitle_data()
{
    containsTextFindsTitle_data();
}

void BasicTest::searchDocumentTextFindsSubtitle()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() > 0);
    qDebug() << string << doc.subtitle;
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::subtitle)).length() > 0);
}

void BasicTest::searchDocumentTextFindsSubtitle_data()
{
    containsTextFindsSubtitle_data();
}

void BasicTest::searchDocumentTextFindsBody()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() > 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::text)).length() > 0);
}

void BasicTest::searchDocumentTextFindsBody_data()
{
    containsTextFindsBody_data();
}

void BasicTest::searchDocumentTextFindsEpoch()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() > 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::epoch)).length() > 0);
}

void BasicTest::searchDocumentTextFindsEpoch_data()
{
    containsTextFindsEpoch_data();
}

void BasicTest::searchDocumentTextFindsEntry()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() > 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::entries)).length() > 0);
}

void BasicTest::searchDocumentTextFindsEntry_data()
{
    containsTextFindsEntry_data();
}

void BasicTest::searchDocumentTextNoResults()
{
    QFETCH(QString, title);
    QFETCH(QString, subtitle);
    QFETCH(QString, text);
    QFETCH(QDateTime, epoch);
    QFETCH(QVector<Document::Entry>, entries);
    QFETCH(QString, string);

    Document doc = Document(title, subtitle, text, epoch);
    doc.entries = entries;

    QVERIFY(doc.searchDocumentText(string, 0).length() == 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::title)).length() == 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::epoch)).length() == 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::subtitle)).length() == 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::text)).length() == 0);
    QVERIFY(doc.searchDocumentText(string, static_cast<int>(Document::searchResultType::entries)).length() == 0);
}

void BasicTest::searchDocumentTextNoResults_data()
{
    containsTextNoResults_data();
}

void BasicTest::equalsReturnsTrue()
{
    Document doc1("", "", "", QDateTime::fromSecsSinceEpoch(1312123));
    Document doc2("", "", "", QDateTime::fromSecsSinceEpoch(1312123));
	QVERIFY(doc1.equals(doc2));
	doc1 = Document("TestDocument", "subtitle", "", QDateTime::fromSecsSinceEpoch(131));
	doc2 = Document("TestDocument", "subtitle", "", QDateTime::fromSecsSinceEpoch(131));
	QVERIFY(doc1.equals(doc2));
	doc1 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(1598888));
	doc2 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(1598888));
    QVERIFY(doc1.equals(doc2));
}

void BasicTest::equalsReturnsFalse()
{
    Document doc1("", "", "", QDateTime::fromSecsSinceEpoch(1312123));
    Document doc2(" ", "", "", QDateTime::fromSecsSinceEpoch(1312123));
    QVERIFY(!doc1.equals(doc2));
    doc1 = Document("TestDocument", "", "szia", QDateTime::fromSecsSinceEpoch(13));
    doc2 = Document("TestDocument", "subtitle", "", QDateTime::fromSecsSinceEpoch(131));
    QVERIFY(!doc1.equals(doc2));
    doc1 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(0));
    doc2 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(1598888));
    QVERIFY(!doc1.equals(doc2));
}

void BasicTest::isLessThanWorks()
{
    QStringList list {"Epoch (Ascending)", "Title (Ascending)"};
    Document doc1("", "", "", QDateTime::fromSecsSinceEpoch(1111111));
    Document doc2(" ", "", "", QDateTime::fromSecsSinceEpoch(2222222));
    QVERIFY(doc1.isLessThan(doc2, list));

    list = QStringList{"Epoch (Descending)", "Title (Ascending)"};
    doc1 = Document("TestDocument", "", "szia", QDateTime::fromSecsSinceEpoch(33333));
    doc2 = Document("TestDocument", "subtitle", "", QDateTime::fromSecsSinceEpoch(111));
    QVERIFY(doc1.isLessThan(doc2, list));

    list = QStringList{"Title (Ascending)", "Epoch (Ascending)"};
    doc1 = Document("ABC", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(0));
    doc2 = Document("ZXW", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(1598888));
    QVERIFY(doc1.isLessThan(doc2, list));

    list = QStringList{"Title (Descending)", "Epoch (Ascending)"};
    doc1 = Document("Test", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(0));
    doc2 = Document("A Test", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(1598888));
    QVERIFY(doc1.isLessThan(doc2, list));
}

void BasicTest::operatorEquals()
{
    Document doc1("", "", "", QDateTime::fromSecsSinceEpoch(1312123));
    doc1.key = "A";
    Document doc2;
    doc2.key = "B";
    doc2 = doc1;
    QVERIFY(doc1.equals(doc2));
    QCOMPARE(doc1.key, doc2.key);
    doc1 = Document("TestDocument", "", "szia", QDateTime::fromSecsSinceEpoch(13));
    doc2 = doc1;
    QVERIFY(doc1.equals(doc2));
    QCOMPARE(doc1.key, doc2.key);
    doc1 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(0));
    doc2 = doc1;
    QVERIFY(doc1.equals(doc2));
    QCOMPARE(doc1.key, doc2.key);
}

void BasicTest::makeEqualToWorks()
{
    Document doc1("", "", "", QDateTime::fromSecsSinceEpoch(1312123));
    doc1.key = "A";
    Document doc2;
    doc2.makeEqualTo(doc1);
    doc2.key = "B";
    QVERIFY(doc1.equals(doc2));
    QVERIFY(doc1.key != doc2.key);
    doc1 = Document("TestDocument", "", "szia", QDateTime::fromSecsSinceEpoch(13));
    doc2.makeEqualTo(doc1);
    QVERIFY(doc1.equals(doc2));
    QVERIFY(doc1.key != doc2.key);
    doc1 = Document("$ß¤ ¨˝", "˝[]€|\\đĐ&@{}][", "\t\t\n\n}q TESZT", QDateTime::fromSecsSinceEpoch(0));
    doc2.makeEqualTo(doc1);
    QVERIFY(doc1.equals(doc2));
    QVERIFY(doc1.key != doc2.key);
}
